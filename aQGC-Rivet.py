theApp.EvtMax = -1

import os
import AthenaPoolCnvSvc.ReadAthenaPool
#This is for Sherpa
#svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/user/n/ngubbi/MC_production/363507_pool.root"]
#This is for MadGraph
#svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/user/n/ngubbi/MG-material/EVNT.pool.root"]

#This for aQGC ##give the path of the input file if its already downloaded else use the output container on the grid and launch jobs such that they take input from grid containers themselves
svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/user/n/ngubbi/aQGC-gridjobs/T0/user.ngubbi.aQGC-T0-7pow12.10k-3lepevents.v2_EXT0/user.ngubbi.15523347.EXT0._000002.EVNT-aQGC-01.pool-T0-7pow12.10k-3lepevents.v2.root"]
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD']
rivet.Analyses += [ 'WWZ' ]
rivet.RunName = ""
rivet.HistoFile = "aQGC-Rivet.yoda"

OutputRoot  = 'aQGC-Rivet.root'
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
from GaudiSvc.GaudiSvcConf import THistSvc
svcMgr += THistSvc()
svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot+"' OPT='RECREATE'"]
#rivet.CrossSection = 1
#rivet.CrossSection = 9.1185E+03
job += rivet



### import AthenaPoolCnvSvc.ReadAthenaPool
### svcMgr.EventSelector.InputCollections = []
### 
### OutputYoda = 'output.yoda'
### 
### import glob, AthenaPoolCnvSvc.ReadAthenaPool
### #svcMgr.EventSelector.InputCollections=[]
### #svcMgr.EventSelector.InputCollections = glob.glob("/afs/cern.ch/user/n/ngubbi/MC_production/363507_pool.root")
### #svcMgr.EventSelector.InputCollections = glob.glob("/afs/cern.ch/user/a/asciandr/work/WWZ/rivet_WWZ/363507_pool.root")
### svcMgr.EventSelector.InputCollections = glob.glob("/afs/cern.ch/user/a/asciandr/work/WWZ/rivet_WWZ/keshav_363507_pool.root")
### 
### from AthenaCommon.AlgSequence import AlgSequence
### job = AlgSequence()
### 
### from Rivet_i.Rivet_iConf import Rivet_i
### rivet = Rivet_i()
### rivet.AnalysisPath = os.environ['PWD']
### rivet.Analyses += ['WWZ']
### #rivet.Analyses += ['ttgamma_eft_dilep']
### rivet.HistoFile = OutputYoda
### job += rivet
### 
### 
### #rivet_dilep = Rivet_i()
### #rivet_dilep.AnalysisPath = os.environ['PWD']
### #rivet_dilep.Analyses += ['ttgamma_eft_dilep']
### #rivet_dilep.HistoFile = OutputYoda_dilep
### #rivet.CrossSection = 1
### #job += rivet_dilep
### 
### #from AthenaCommon.AppMgr import ServiceMgr as svcMgr
### #from GaudiSvc.GaudiSvcConf import THistSvc
### #svcMgr += THistSvc()
### #svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot_dilep+"' OPT='RECREATE'"]
### 
### 
### #svcMgr += THistSvc()
### #svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot_dilep+"' OPT='RECREATE'"]
### 
### #from AthenaCommon.AppMgr import ServiceMgr as svcMgr
### #from GaudiSvc.GaudiSvcConf import THistSvc
### #svcMgr += THistSvc()
### #svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot+"' OPT='RECREATE'"]
