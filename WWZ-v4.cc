//to compile this rivet file : 1. setupATLAS 2. asetup 20.20.8.3,here 3.rivet-buildplugin RivetWWZ-v4.so WWZ-v4.cc
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"

namespace Rivet {


  class WWZ : public Analysis {
  public:
    int counter=0,jet_counter=0,lep_counter=0,tau_neutrino_counter=0,neutrino_event_counter=0;

    /// Constructor
    WWZ() : Analysis("WWZ") {    }

    /// @name Analysis methods
    /// Book histograms and initialise projections before the run
    void init() {
   
      //****************init() FOR W->jj*************************************
      //Charged Leptons within acceptance-for W ; Neutrinos,b-hadrons not included in FS
      Cut fs_z = Cuts::abseta < 5 && Cuts::pT >0.1*GeV; //Loose Cuts
      // Cut fs_z = Cuts::abseta <2.5 && Cuts::pT >15*GeV;

      // const PromptFinalState chLep_fid = PromptFinalState(fs_z && ( Cuts::abspid == PID::ELECTRON || Cuts::abspid == PID::MUON ));
      const PromptFinalState chLep_fid = PromptFinalState(fs_z && ( Cuts::abspid == PID::ELECTRON || Cuts::abspid == PID::MUON ), false, false );
      const PromptFinalState photon_fs = PromptFinalState ( Cuts::abspid == PID::PHOTON);// To get Photons to dress leptons 
      const DressedLeptons dressed_leps(photon_fs,chLep_fid,0.1,Cuts::pT>0.1*GeV);
      declare(dressed_leps,"Dressed Leptons");
      //Jets,ANti-kt 0.4
      VetoedFinalState fsJets(FinalState(Cuts::abseta<7.0));//final state for jet finding: veto leptons and neutrinos
      fsJets.vetoNeutrinos();
      fsJets.addVetoOnThisFinalState(chLep_fid);
      declare(FastJets(fsJets,FastJets::ANTIKT,0.4),"Jets");
      
      //****************init() FOR WZ*************************************
      const FinalState fs;
                                                                                                                 
      //Lepton Cuts
      //Cut FS_Zlept = Cuts::abseta < 2.5 && Cuts::pT > 15*GeV;
      Cut FS_Zlept = Cuts::abseta < 5 && Cuts::pT > 0.1*GeV;//Loose Cuts
      //Cut fs_z = Cuts::abseta < 2.5 && Cuts::pT > 15*GeV;
      Cut fs_j = Cuts::abseta < 5 && Cuts::pT > 2.5*GeV;
      
      //Get photons to dress leptons-for Z                                                                                                                                                                 
      PromptFinalState photons(Cuts::abspid == PID::PHOTON);

      //Electrons and muons in Fiducial PS- for Z;Total PS exluded for now                                                                                                                      
      //PromptFinalState leptons (Cuts::abspid == PID::ELECTRON || Cuts::abspid == PID::MUON);
      PromptFinalState leptons( fs_z && (Cuts::abspid == PID::ELECTRON || Cuts::abspid == PID::MUON), true, false );
      // PromptFinalState leptons( Cuts::abspid == PID::ELECTRON || Cuts::abspid == PID::MUON,bool accepttaudecays=false, bool acceptmudecays=false ); 
      leptons.acceptTauDecays(true);
      DressedLeptons dressedleptons(photons, leptons, 0.1, FS_Zlept, true);
      addProjection(dressedleptons, "DressedLeptons");
      //Neutrinos
      IdentifiedFinalState nu_id;
      nu_id.acceptNeutrinos();
      PromptFinalState neutrinos(nu_id);
      // neutrinos.acceptTauDecays(false);
      neutrinos.acceptTauDecays(true);
      declare(neutrinos, "Neutrinos");
      MSG_WARNING("LIMITED VALIDITY - check info file for details!\033[m");
      //Jets-For Z
      VetoedFinalState veto;
      veto.addVetoOnThisFinalState(dressedleptons);
      FastJets jets(veto, FastJets::ANTIKT, 0.4);
      declare(jets, "JetsZ");


      /// Book histograms
   
      // For W -> jj
      _h_Njets          = bookHisto1D("Njets"        ,12   ,  0, 12, "Number of Jets"                  , "N_jets"               , "(1/N) n. of events");
      _h_DiJetMass1     = bookHisto1D("DiJetMass1"   ,3000 , 60,160,"Mass from DiJet pair -DiJetMass1", "DiJetMass1 [GeV]", "(1/N) n. of events");     
      _h_DiJetMass2     = bookHisto1D("DiJetMass2"   ,3000 , 20,500,"Mass from DiJet pair -DiJetMass2", "DiJetMass2 [GeV]", "(1/N) n. of events");
      _h_singlejet_m    = bookHisto1D("singlejet_m"  ,3000 , 70,140, "Invariant mass from single Jet", "singlejet_mass [GeV]" , "(1/N) n. of events"); 
      _h_DiJet_pt_BestPair    = bookHisto1D("DiJet_pt_BestPair",4000,  0, 1000,"DiJet_pt_BestPair"     , "DiJet_pt_BestPair[GeV] ", "(1/N) n. of events");
      _h_DiJet_pt_NonBestPair    = bookHisto1D("DiJet_pt_NonBestPair",4000,  0, 2000,"DiJet_pt_NonBestPair"     , "DiJet_pt_NonBestPair[GeV] ", "(1/N) n. of events");
      _h_delta_eta_BestPair = bookHisto1D("delta_eta_BestPair",200,0,3.14, "delta_eta_BestPair" , "delta_eta_BestPair", "(1/N) n. of events");
      _h_delta_eta_NonBestPair = bookHisto1D("delta_eta_NonBestPair",200,0,3.14, "delta_eta_NonBestPair" , "delta_eta_NonBestPair", "(1/N) n. of events");
      _h_delta_phi_BestPair = bookHisto1D("delta_phi_BestPair",200,0,3.14, "delta_phi_BestPair" , "delta_phi_BestPair", "(1/N) n. of events");
      _h_delta_phi_NonBestPair = bookHisto1D("delta_phi_NonBestPair",200,0,3.14, "delta_phi_NonBestPair" , "delta_phi_NonBestPair", "(1/N) n. of events");
      _h_ReconMass_BestPair = bookHisto1D("ReconMass_BestPair",3000,70,100, "ReconMass_BestPair", "ReconMass_BestPair[GeV]","(1/N) n. of events");
      _h_ReconMass_NonBestPair = bookHisto1D("ReconMass_NonBestPair",4500,0,1500, "ReconMass_NonBestPair", "ReconMass_NonBestPair[GeV]","(1/N) n. of events");
      //Initialising histograms for WZ->3l,1v  
      _h_Wboson_m   = bookHisto1D("Wboson_m"    ,3000, 70, 140, "Invariant Mass of l,nu","W mass [GeV]","(1/N) n. of events");
      _h_Zboson_m   = bookHisto1D("Zboson_m"    ,3000, 70, 140, "Invariant Z ofl+l-","Z mass [GeV]","(1/N) n. of events");
      // _h_Wboson_mT  = bookHisto1D("Wboson_mT"   ,200, 0, 200, "Transverse W Mass","W Transverse mass [GeV]","(1/N) n. of events");
      // _h_Zboson_mT  = bookHisto1D("Zboson_mT"   ,100, 0, 500, "n. of events","Zboson Transverse mass","(1/N) n. of events");     
   
      _h_Wboson_pt  = bookHisto1D("Wboson_pt"   ,4000, 0, 1000, "Wboson pT","Lep_W_pT [GeV]","(1/N) n. of events");
      _h_Zboson_pt  = bookHisto1D("Zboson_pt"   ,4000, 0, 1000, "Zboson pT","Z_pT [GeV]"    ,"(1/N) n. of events");
     
      _h_Wboson_eta = bookHisto1D("Wboson_eta"          ,31 ,0, 3.14, "eta for Lep_W"    ,"Lep_W_eta","(1/N) n. of events");
      _h_Zboson_eta = bookHisto1D("Zboson_eta"          ,31 ,0, 3.14, "eta for Z"        ,"Z_eta"    ,"(1/N) n. of events");
      _h_Hadronic_W_eta = bookHisto1D("Hadronic_W_eta"  ,31 ,0, 3.14, "eta for Had_W"    ,"Had_W_eta","(1/N) n. of events");
    
      _h_Wboson_Phi = bookHisto1D("Wboson_Phi"          ,31 ,0, 3.14, "Phi for Lep_W","Lep_W_Phi","(1/N) n. of events");
      _h_Hadronic_W_Phi = bookHisto1D("Hadronic_W_Phi"  ,31 ,0, 3.14, "Phi for Z"    ,"Had_W_Phi","(1/N) n. of events");
      _h_Zboson_Phi = bookHisto1D("Zboson_Phi"          ,31 ,0, 3.14, "Phi for Had_W","Z_Phi"    ,"(1/N) n. of events");

      _h_H_t      = bookHisto1D("H_t"             ,4000, 0, 1000, "H_t [GeV]","H_t","(1/N) n. of events");
      _h_nu_pT      = bookHisto1D("nu_pT"         ,4000, 0, 1000, "nu_pT [GeV]","Neutrino Transverse Momentum","(1/N) n. of events");
      //  _h_theta_ll      = bookHisto1D("theta_ll"   ,200, 0, 3.14, "Angle theta between l+ and l-","theta l+l-","(1/N) n. of events");
      // _h_mTWZ       = bookHisto1D("_mTWZ"       ,300, 0, 300, "n. of events","WZ Transverse mass","(1/N) n. of events");
      _h_Ntotalevents = bookHisto1D("Ntotalevents",3,0,2,"total n. of events","Nevents without any selections","(1/N) n. of events");
 
    
      _h_Wlepton_pt = bookHisto1D("Wlepton_pt", 4000, 0, 1000, "pT for lepton from W","pT [GeV]","(1/N) n. of events");
      _h_Zlepton_pt = bookHisto1D("Zlepton_pt", 4000, 0, 1000, "pT for leptons from Z","pT [GeV]","(1/N) n. of events");
      _h_delta_Eta_lplm  = bookHisto1D("delta_Eta_lplm"    ,31 ,0, 3.14, "Delta Eta between l+l-","Delta Eta l+l-","(1/N) n. of events");
      _h_delta_Phi_lplm =bookHisto1D("delta_Phi_lplm"      ,31 ,0, 3.14, "Delta Phi between l+l-","Delta Phi l+l-","(1/N) n. of events");
      _h_delta_first_Eta  = bookHisto1D("delta_first_Eta"  ,31 ,0, 3.14, "Delta Eta between Leptonic_W and Z"         ,"Delta Eta","(1/N) n. of events");
      _h_delta_second_Eta  = bookHisto1D("delta_second_Eta",31 ,0, 3.14, "Delta Eta between Leptonic_W and Hadronic_W","Delta Eta","(1/N) n. of events");
      _h_delta_third_Eta  = bookHisto1D("delta_third_Eta"  ,31 ,0, 3.14, "Delta Eta between Hadronic_W and Z"         ,"Delta Eta","(1/N) n. of events");
    
      _h_delta_first_Phi  = bookHisto1D("delta_first_Phi"  ,31,0,3.14, "Delta Phi between Leptonic W and Z"         ,"delta Phi","(1/N) n. of events");
      _h_delta_second_Phi  = bookHisto1D("delta_second_Phi",31,0,3.14, "Delta Phi between Leptonic_W and Hadronic_W","delta Phi","(1/N) n. of events");
      _h_delta_third_Phi  = bookHisto1D("delta_third_Phi"  ,31,0,3.14, "Delta Phi between Hadronic_W and Z"         ,"delta Phi","(1/N) n. of events");

      _h_delta_R_first =bookHisto1D("delta_R_first  "      ,31,0,5, "Delta R between Leptonic_W and Z"         ,"delta R","(1/N) n. of events" );
      _h_delta_R_second =bookHisto1D("delta_R_second"      ,31,0,5, "Delta R between Leptonic_W and Hadronic_W","delta R","(1/N) n. of events" );
      _h_delta_R_third =bookHisto1D("delta_R_third  "      ,31,0,5, "Delta R between Hadronic_W and Z"         ,"delta R","(1/N) n. of events" );


      _h_Nlepevents_after = bookHisto1D("Nlepevents_after"       ,3 ,0 ,10000,"n. of events","Nevents after lepton selections","(1/N) n. of events");
      _h_Njetevents       = bookHisto1D("Njetevents"             ,3 ,0 ,10000 ,"n. of events","Nevents of jet","(1/N) n. of events");
      _h_Nlep_before      = bookHisto1D("Nlep_before"            ,5 ,0 ,5,      "n. of events","Number of Leptons before selection","(1/N) n. of events");
      _h_Nlep_after       = bookHisto1D("Nlep_after"             ,5 ,0 ,5,      "n. of events","Number of Leptons after selection","(1/N) n. of events");
      _h_Nnu              = bookHisto1D("Nnu"                    ,7,0 ,7,    "Number of Neutrinos","N_nu","(1/N) n. of events");

    }
    /// Perform the per-event analysis
    void analyze(const Event& event) {
      counter++;
      _h_Ntotalevents->fill(0);
      //cout<<"counter="<<counter<<endl;
      /// @todo Do the event by event analysis here
      //Variables Initialization
      const double weight = event.weight();
      FourMomentum Hadronic_W,Zlepton1,Zlepton2,Wlepton;
      //Histogram with No selections, just giving total number of events
      // _h_Ntotalevents->fill(100.0,1);

      // ****************Jets Initialization*************************************
      //Get Leptons
      vector<DressedLepton> leps = apply<DressedLeptons>(event,"DressedLeptons").dressedLeptons();

      //Get Jets
      const Jets& jets = apply<FastJets>(event,"Jets").jetsByPt(Cuts::pT>10*GeV);//This one works!The Baseline Cut!
      //cout<<"jets="<<jets<<endl;
      //Jets analysis
      double Njets = jets.size();
      //cout<< "Njets="<<Njets<<endl;
      double ReconMass_BestPair(-1),DiJetMass1(-1),DiJet_pt_BestPair(-1),delta_eta_BestPair(-1),delta_phi_BestPair(-1),W1(-1),W2(9999999999999),singlejet_m(-1);
      //double ReconMass_NonBestPair(-1),DiJetMass2(-1),delta_eta_NonBestPair(-1),delta_phi_NonBestPair(-1),DiJet_pt_NonBestPair(-1);
      FourMomentum DiJet_BestPair,BestJet1,BestJet2;
      //DiJet_NonBestPair,NonBestJet1,NonBestJet2;       
      //###################Leptond and Neutrinos Initialization#########################################
      int i, j, k,Ntau(-1),d(0),f(0);
      //obtain dressed leptons and neutrinos for WZ system                                                                                                                       
      const vector<DressedLepton>& dressedleptons = apply<DressedLeptons>(event, "DressedLeptons").dressedLeptons();
      const Particles& neutrinos = apply<PromptFinalState>(event, "Neutrinos").particlesByPt();
      Jets jets_Z = apply<JetAlg>(event, "Jets").jetsByPt( (Cuts::abseta < 5) && (Cuts::pT > 2.5*GeV) );//Do we need this at all, since we arent interested with Jets from Z?    
      //int EventType = -1;                                                                                                                                                     
      int Nel = 0, Nmu = 0,Nlep_before=0,Nlep_after=0,Nnu=0;//Number of various leptons and neutrinos 
      //#################Jet Reconstruction and Analysis########################################################
      //Check for Njets<2. If its 0 jets , display error message but if its >0 and ==1, then obtain the Invariant mass of the single jet; Also, fill Wjj_Njets Histo to count for number of jets so that
      bool anyGood = false;
      //events with one jet is also included.
      //if(Njets<2){ continue;
	//{
	//singlejet_m = (Njets==0) ? -99. : (jets[0].momentum()).mass();
	//cout<<"singlejet Invariant mass= \n"<<singlejet_m<<endl;
	//_h_singlejet_m->fill(singlejet_m>100?100:singlejet_m,weight);
	//_h_singlejet_m->fill(singlejet_m>139?139:singlejet_m,weight);//overflow for singlejet_m
       	//cout<<"less than two jets"<<endl;
	//_h_Wjj_Njets-> fill(Njets,weight);	
	//vetoEvent;
	// }
							      
      if(Njets>=2){
	//cut1 for Jets/ Make it a better version!
	jet_counter++;
	//_h_Ntotalevents->fill(1);
	//cout<<"jet_counter="<<jet_counter<<endl;
	//cout<<"Number of Jets= \n"<<Njets<<endl;
       	_h_Njetevents->fill(1.0,1);//To obtain the number of events after jet selections of having atleast two jets
	_h_Njets-> fill(Njets,weight);//To obtain number of jets
	//cout<<"###########New Jet Loop Begins###################: "<<endl;
	//Loop over a given pair of jets; Obtain invariant mass from them and fill into respective histogram
	for( int a =0;a<Njets;a++) {
	  for(int b =0;b<Njets;b++) {
	    if( jets[a].momentum().pt()<=20){
	      //cout<<"Jet_a low pT- less than 20GeV= "<<(jets[a].momentum()).pt()<<endl;
	      continue;
	    }
	    if( jets[b].momentum().pt()<=20 ){
	      //cout<<"Jet_b low pT- less than 20GeV= "<<(jets[b].momentum()).pt()<<endl;
	      continue;
	    }
	    if (a>=b) continue;
	    DiJetMass1 = (jets[a].momentum() + jets[b].momentum()).mass();//Mass from DiJet system of Pair of Jets; We havent classified it if its BestPair of Jets or not, still at this point
	    //cout<<" DiJetMass1 = "<< DiJetMass1<<endl;
	    W1 = abs( DiJetMass1 - MW_PDG);//Difference between  DiJetMass1 and MW_PDG in order to obtain a value close to Wmass
	    //cout<<"W1= "<<W1<<endl;
	    if (W1<=W2 && W1<20){
	      W2=W1;
	      //cout<<"W2= "<<W2<<endl;
	      //cout<<"FOUND BESTPAIR of jets !! "<<endl;
	      d =a;           
	      f=b; 
	      //cout<<"Best Pair of Jets selected is = "<<d<<f<<endl;
	      anyGood = true;
	      BestJet1 = jets[d].momentum();//These are the two individual jets with respective indices stored in d,f that are the best pair of jets!                          
	      BestJet2 = jets[f].momentum();
	      //cout<<"Best Pair of Jets selected is: Jet1_Index= "<<d<<",Jet2_index="<<f<<endl;
	      DiJet_BestPair = jets[d].momentum() + jets[f].momentum();//DiJet_BestPair  
	      //Since this is the best Pair of Jets, I have to get Eta, PT and Phi from them at this point itself                                                              
	      ReconMass_BestPair = (DiJet_BestPair).mass();
	      //cout<<"ReconMass_BestPair= "<<ReconMass_BestPair<<endl;
	      DiJet_pt_BestPair = (DiJet_BestPair).pt()*GeV;
              delta_eta_BestPair = Pi -abs(abs(BestJet1.eta() - BestJet2.eta() ) - Pi);
              delta_phi_BestPair = Pi -abs(abs(BestJet1.phi() - BestJet2.phi() ) - Pi);
              //cout<<"Finally  DiJetMass1 chosen= "<<DiJetMass1<<endl;
              //cout<<"Finally  ReconMass_BestPair chosen= "<<ReconMass_BestPair<<endl;
	      //    cout<<"delta_eta_BestPair= "<<delta_eta_BestPair<<endl;
	      // cout<<"delta_phi_BestPair= "<<delta_phi_BestPair<<endl;
	    }
	    else{
	      //      cout<<"NOT suitable to be BESTPAIR of jets !! "<<endl;
	      continue;
	    } 
	  }
	} 
	  /* else{
	    cout<<"Mass is crappy->NOT THE BEST PAIR OF JETS"<<endl;// All the jets in this loop are the NonBest Jets that didnt pass earlier jet selections!
	    cout<<"Moving to-> NonBestPair of Jets "<<endl;
	    NonBestJet1 = jets[a].momentum();//All the other pair of jets that did not meet the earlier conditions mentioned at Conditions-MasterShifu1 are NonBestpair jets
	    NonBestJet2 = jets[b].momentum();
	    DiJetMass2 = (jets[a].momentum() + jets[b].momentum()).mass();
	    // W2 = abs(DiJetMass2 - MW_PDG);
	    //Conditions-MasterShifu2
	    // if((W2<15) == b1){
	    if( (jets[a].momentum()).pt() >20){
	      cout<<"a in Shifu2  = "<<a<<endl;
	      cout<<"Jet_b Mass in Shifu2 = "<<(jets[a].momentum()).mass()<<endl;
	      if( (jets[b].momentum()).pt() >20){
		    cout<<"b in Shifu2  = "<<b<<endl;
		    cout<<"Jet_b Mass in Shifu2 = "<<(jets[b].momentum()).mass()<<endl;
		    W2 = DiJetMass2;
		    anyGood = true;
		    ReconMass_NonBestPair = DiJetMass2 > ReconMass_NonBestPair ? DiJetMass2 : ReconMass_NonBestPair;
		    cout<<"ReconMass_NonBestPair= "<<ReconMass_NonBestPair<<endl;
	  */
	//cout<<"Finally  DiJetMass1 chosen= "<<DiJetMass1<<endl;                                                                                                      
	//	    cout<<"Finally  ReconMass_BestPair chosen= "<<ReconMass_BestPair<<endl;                                                                                  
	//	    cout<<"delta_eta_BestPair= "<<delta_eta_BestPair<<endl;
	//	    cout<<"delta_phi_BestPair= "<<delta_phi_BestPair<<endl;
	//	    cout<<"Jet Pair Loop Ends "<<endl;
      }  
      if (!anyGood) vetoEvent;
      //###################NEUTRINOS Analysis#########################################################
      // cout<<"###########Neutrino Loop Begins###################: "<<endl;
      //Obtain number of neutrinos and fill histo
      Nnu = neutrinos.size();
      // cout<<"Total number of neutrinos Nnu ="<<Nnu<<endl;
      _h_Nnu->fill(Nnu,weight);
      if (Nnu>0){      
	neutrino_event_counter++;
      }
      //cout<<"Total Number of neutrino events ="<<neutrino_event_counter<<endl;
      double neutrino_PID,neutrino_pt=0,max_neutrino_pt=0,Ntau_neutrinos=0;
      for(j=0;j<Nnu;j++){
	if (j>Nnu) continue;
        neutrino_PID = neutrinos[j].pid();
        //cout<<"Neutrino_PID ="<<neutrino_PID<<endl;                                                                                                                            
	double reset_counter = 0;
	if( neutrinos[j].pid()==16 ||neutrinos[j].pid()==-16 ){
	  reset_counter++;
	  Ntau_neutrinos++;
	  if (reset_counter>1)continue;
	}
      }
      if (Ntau_neutrinos>0){
	tau_neutrino_counter++;
      }
      //cout<<"Total number of neutrinos Nnu ="<<Nnu<<endl;                                                                                                   
      //cout<<"Total number of Tau neutrinos ="<<Ntau_neutrinos<<endl;
      // cout<<"Number of Tau neutrino events ="<<tau_neutrino_counter<<endl;
   
      for (i=0;i<Nnu;i++)
        {
          if (i>Nnu) continue;
          neutrino_pt = (neutrinos[i].momentum()).pt();
          //cout<<"neutrino_pt ="<<neutrino_pt<<endl;                                                                                                                            
          //if (k>Nnu) continue;                                                                                                                                                 
          if ( neutrino_pt > max_neutrino_pt){
            max_neutrino_pt = neutrino_pt;
	  }
	}
      _h_nu_pT->fill(neutrino_pt>999?999:neutrino_pt, weight);
      // cout<<"###########Neutrino Loop ends###################: "<<endl;
      //####################Neutrino Analysis End#################################################
      //cout<<"###########Lepton Loop Begins###################: "<<endl;
      //###########Leptons Analysis###########################################
      for (const DressedLepton& l : dressedleptons)
	{

	  if (l.abspid() == 11)   //electron 
	    ++Nel;
	  if (l.abspid() == 13)  //Muon
	    ++Nmu;
	  if (l.abspid() == 15)  //Tau                                                                                                                                                               
	    ++Ntau;
	  //  cout<<"PDGId of leps are= "<<l.abspid()<<endl;//Display PDG ID to be sure what leptons we are getting
	}
      //Determining number of e,mu and nu, tau
      // cout<<"Number of taus =  "<<Ntau<<endl;
      Nlep_before = Nel+Nmu;//Count number of leptons before making the 3lepton selections and fill histo
      //cout<<"Total number of leptons Nlep before 3lep selection="<<Nlep_before<<endl;
      //_h_Nlep_before-> fill(Nlep_before,weight);
      // _h_lepevents_before->fill(100.0,1);    
 
      //How to find the number of lepton events in various combinations havinga total of 3 leptons between electrons and muons;This is the 3LEPTON SELECTION!
      if      ( Nel == 3  && Nmu==0 )  { //EventType = 3;// cout << "Event Type = 3;\n"<<"Number of e = 3,Number of mu = 0"<< endl;
      } 
      else if ( Nel == 2  && Nmu==1 )  { //EventType = 2;// cout << "Event Type = 2;\n"<<"Number of e = 2,Number of mu = 1"<< endl;
      } 
      else if ( Nel == 1  && Nmu==2 )  { //EventType = 1;// cout << "Event Type = 1;\n"<<"Number of e = 1,Number of mu = 2"<< endl;
      } 
      else if ( Nel == 0  && Nmu==3 )  { //EventType = 0;// cout << "Event Type = 0;\n"<<"Number of e = 0,Number of mu = 3"<< endl;
      } 
      else{
	vetoEvent;//Especially important to veto events to discard any events that doesnt have these combinations so that we get correct number of lepton events after passing           3lepton selections
      }
      Nlep_after = Nel+Nmu;//Count number of leptons after making the 3lepton selections and fill histo
      //cout<<"Total number of leptons Nlep after 3lep selection="<<Nlep_after<<endl;       
      _h_Nlep_after-> fill(Nlep_after,weight); 
      //_h_Nlepevents_after->fill(3.);//Counts the number of events after making jet selections and 3lep selections;these events are those that have atleast 2jets and 3leps
      lep_counter++;
      _h_Ntotalevents->fill(3.);
      //cout<<"lep_counter="<<lep_counter<<endl;
     
      //int EventCharge = -dressedleptons[0].charge()*dressedleptons[1].charge()*dressedleptons[2].charge();
      //cout<<"Event charge = \n"<<EventCharge<<endl;

      //Determining Charge and PID of these leptons                                                                                                                                                         
      // double lepton1_charge = dressedleptons[0].charge();
      // double lepton1_ParticleID = (dressedleptons[0]).pid();
      // cout<<"lepton1_charge =\n"<<lepton1_charge<<endl;
      // cout<<"lepton1_ParticleID =\n"<<lepton1_ParticleID<<endl;

      // double lepton2_charge = (dressedleptons[1]).charge();
      // double lepton2_ParticleID = (dressedleptons[1]).pid();
      // cout<<"lepton2_charge =\n"<<lepton2_charge<<endl;
      // cout<<"lepton2_ParticleID =\n"<<lepton2_ParticleID<<endl;

      // double lepton3_charge = (dressedleptons[2]).charge();
      // double lepton3_ParticleID = (dressedleptons[2]).pid();
      // cout<<"lepton3_charge =\n"<<lepton3_charge<<endl;
      // cout<<"lepton3_ParticleID =\n"<<lepton3_ParticleID<<endl;	 
      int l1=99,l2=99,l3=0;
      double temp_Z = 1000000,mass_Z;
      for (i=0;i<Nlep_after;i++){
	for (j=i+1;j<Nlep_after;j++){
	  // cout<<"i= "<<i<<endl;
	  //cout<<"j= "<<j<<endl;
	  double lepton1_charge = dressedleptons[i].charge();
	  double lepton2_charge = dressedleptons[j].charge();  
	  if(lepton1_charge == lepton2_charge){
	    // cout<<"lepton1_charge =  "<<lepton1_charge<<endl;
	    // cout<<"lepton2_charge =  "<<lepton2_charge<<endl;
	    continue;
	  }
	  double lepton1_ParticleID = (dressedleptons[i]).pid();
	  double lepton2_ParticleID = (dressedleptons[j]).pid();
	  if(lepton1_ParticleID == lepton2_ParticleID){
	    //cout<<"lepton1_ParticleID =  "<<lepton1_ParticleID<<endl;
	    //cout<<"lepton1_ParticleID =  "<<lepton1_ParticleID<<endl;
	    continue;
	  }
	  mass_Z = (dressedleptons[i].momentum() + dressedleptons[j].momentum()).mass();
	  // cout<<"mass_Z="<<mass_Z<<endl;
	  if( (mass_Z - MZ_PDG) < temp_Z){
	    temp_Z = mass_Z;
	    //cout<<"temp_Z="<<temp_Z<<endl;
	  }
	  l1 =i;
	  l2=j;
	}
      }
      for (k=0;k<3;k++){
	if(l3 ==l1||l3==l2){
	  l3++;
	}
	//l3 =k;
      }
      //cout<<"l1= "<<l1<<endl;//l1,l2 come from Z and l3 comes from W
      //cout<<"l2= "<<l2<<endl;
      //cout<<"l3= "<<l3<<endl;
      //################pT of leptons#######################
      //Wlepton is the lepton originating from W ##Star B
      double Wlepton_pt = (dressedleptons[l3].momentum()).pt()*GeV;
      if(Wlepton_pt<=20){
	vetoEvent;
      } 
      // double Wlepton_pt = (Wlepton).pt()*GeV;
      //cout<<"Wlepton_pt= \n"<<Wlepton_pt<<endl;
      // _h_Wlepton_pt->fill(Wlepton_pt, weight);
      // _h_Wlepton_pt->fill(Wlepton_pt>200?200:Wlepton_pt, weight);
      //_h_Wlepton_pt->fill(Wlepton_pt>999?999:Wlepton_pt, weight);//overflow for Lepton from Leptonic_W Pt
      //Zlepton1,2 are the leptons originating from Z                                                                                                                           
      // double Zlepton1_pt = (dressedleptons[l1].momentum()).pt()*GeV; 
      //double Zlepton2_pt = (dressedleptons[l2].momentum()).pt()*GeV;
      
      double Zlepton_pt = (dressedleptons[l2].momentum() + dressedleptons[l2].momentum()).pt()*GeV;  
      if(Zlepton_pt<=20){
	vetoEvent;
      }
      //double Zlepton1_pt = (Zlepton1).pt()*GeV;
      //double Zlepton2_pt = (Zlepton2).pt()*GeV;
      //cout<<"Zlepton_pt= \n"<<Zlepton_pt<<endl;
      //cout<<"Zlepton2_pt= \n"<<Zlepton2_pt<<endl;
      // _h_Zlepton_pt->fill(Zlepton_pt>999?999:Zlepton_pt, weight);// Making a single entiry per event.  
      // _h_Zlepton_pt->fill(Zlepton1_pt>999?999:Zlepton1_pt, weight);//StarB //overflow for Leptons from Z. Making a double entires per event.   
      // _h_Zlepton_pt->fill(Zlepton1_pt>200?200:Zlepton1_pt, weight);
      // _h_Zlepton_pt->fill(Zlepton2_pt>999?999:Zlepton2_pt, weight);
      // _h_Zlepton_pt->fill(Zlepton2_pt>200?200:Zlepton2_pt, weight);
    
      //Obtaining Eta and Delta Eta between l+l- coming from Z
      double Eta_l1 = (dressedleptons[l1].momentum()).eta();//Star#6                                                                                                             
      // cout<<"Eta_lplus= \n "<<Eta_lplus<<endl;
      double Eta_l2 = (dressedleptons[l2].momentum()).eta();                                                                                                                     
      // cout<<"Eta_lminus= \n "<<Eta_lminus<<endl;
      double delta_Eta_lplm = Pi- abs(abs(Eta_l1 - Eta_l2)-Pi); 
      // cout<<"delta_Eta_lplm= \n "<<delta_Eta_lplm<<endl;
      //_h_delta_Eta_lplm->fill(delta_Eta_lplm,weight); 
      
      //Obtaining Phi and Delta Phi between l+l- coming from Z                                                                                                                  
      double Phi_l1 = (dressedleptons[l1].momentum()).phi();//Star#7                                                                                                          
      //cout<<"Phi_lplus= \n "<<Eta_lplus<<endl;
      double Phi_l2 = (dressedleptons[l2].momentum()).phi();
      //cout<<"Phi_lminus= \n "<<Phi_lminus<<endl;
      double delta_Phi_lplm = Pi - abs( abs(Phi_l1 - Phi_l2) - Pi);
      //cout<<"delta_Phi_lplm= \n "<<delta_Phi_lplm<<endl;                                                                                                                     
      //_h_delta_Phi_lplm->fill(delta_Phi_lplm,weight);

      //obtain the four momentum of Z, W bosons ##starA
      // cout << neutrinos.size()
      FourMomentum Zboson   = dressedleptons[l1].momentum()+dressedleptons[l2].momentum();
      FourMomentum Wboson   = dressedleptons[l3].momentum()+neutrinos[0].momentum();
      //cout<<"Zboson = \n"<<Zboson<<endl;
      //cout<<"Wboson = \n"<<Wboson<<endl;
      //Hadronic_W //Star#4
      Hadronic_W =  DiJet_BestPair;
      //cout<<"Hadronic_W = \n"<<Hadronic_W<<endl;

      //Obtain mass and transverse mass of W and Z bosons,along with phi and eta and fill into respective histos
      double Wboson_m = (Wboson).mass()*GeV;
      double Zboson_m=  (Zboson).mass()*GeV; 
      //  double Wboson_mT = sqrt( 2 * Wlepton.pT() * neutrinos[0].pt() * (1 - cos(deltaPhi(Wlepton, neutrinos[0]))) )/GeV;//W Boson Transverse mass
      //cout<<"Wboson_m  = \n"<<Wboson_m<<endl;    
      //cout<<"Zboson_m  = \n"<<Zboson_m<<endl;
      //cout<<"Wboson_mT = \n"<<Wboson_mT<<endl;
      // _h_Wboson_m->fill(Wboson_m<41?41:Wboson_m, weight);//underflow for Invariant Mass for Leptonic_W
      //_h_Wboson_m->fill(Wboson_m>139?139:Wboson_m, weight);//overflow for Invariant Mass for Leptonic_W 
      // _h_Zboson_m->fill(Zboson_m<41?41:Zboson_m, weight);//underflow for Invariant Mass for Z
      //_h_Zboson_m->fill(Zboson_m>139?139:Zboson_m, weight);//overflow for  Invariant Mass for Z 
      // _h_Wboson_mT->fill(Wboson_mT>100?100:Wboson_mT, weight);

      // relaxing these cuts
      //  if (fabs(Zboson.mass()-MZ_PDG)>=10.)  vetoEvent;
      // if (Wboson_mT<=30.)                   vetoEvent;
      // if (Wlepton.pT()<=20.)                vetoEvent;
      // if (deltaR(Zlepton1,Zlepton2) < 0.2)  vetoEvent;
      // if (deltaR(Zlepton1,Wlepton)  < 0.3)  vetoEvent;
      // if (deltaR(Zlepton2,Wlepton)  < 0.3)  vetoEvent;
     
      //Obtain pT of W and Z bosons and fill into respective histos   
      double Wboson_pt = (Wboson).pt()*GeV;
      double Zboson_pt = (Zboson).pt()*GeV;
      //cout<<"Wboson_pt = \n"<<Wboson_pt<<endl;
      //cout<<"Zboson_pt = \n"<<Zboson_pt<<endl;
      // _h_Wboson_pt->fill(Wboson_pt, weight);
      // _h_Zboson_pt->fill(Zboson_pt, weight);
      // _h_Wboson_pt->fill(Wboson_pt>999?999:Wboson_pt, weight); //Overflow for pT Leptonic_W                                                                                         // _h_Zboson_pt->fill(Zboson_pt>999?999:Zboson_pt, weight);//Overflow for pT of Z                                                  
      
      //Calculating eta, Phi and deltaPhi,deltaEta,deltaR of and between the three bosons     
      //Star#1//Calculating Eta                                                                                                                                                  
      double eta1 = (Wboson).eta();
      //cout<<"Wboson_eta= \n"<<eta1<<endl;
      // _h_Wboson_eta->fill(eta1,weight);
      double eta2 = (Zboson).eta();
      //cout<<"Zboson_eta= "<<eta2<<endl;                                                                                                                                       
      // _h_Zboson_eta->fill(eta2,weight);  
      double eta3 = (Hadronic_W).eta();//Star#5
      //cout<<"Hadronic_W_eta= \n "<<eta3<<endl;
      //_h_Hadronic_W_eta->fill(eta3,weight);

      //Calculating delta_Eta 
      //Eta between leptonic_W and Z
      // double delta_first_Eta = (Wboson + Zboson).deltaEta();
      double delta_first_Eta =Pi-abs( abs(eta1 - eta2)-Pi);
      // cout<<"delta_first_Eta"<<delta_first_Eta<<endl;
      //_h_delta_first_Eta->fill(delta_first_Eta,weight);

      //Eta between Hadronic_W and Leptonic_W                                                                                                                                          // double delta_second_Eta = (Hadronic_W + Wboson).deltaEta();
      double delta_second_Eta = Pi- abs(abs( eta3 - eta1)-Pi);
      //cout<<"delta_second_Eta=  \n"<<delta_second_Eta<<endl;
      //_h_delta_second_Eta->fill(delta_second_Eta,weight);

      //Eta between Hadronic_W and Z                                                                                                                                           
      // double delta_third_Eta = (Hadronic_W + Zboson).deltaEta();
      double delta_third_Eta =Pi-abs(abs( eta3 - eta2)-Pi);
      //cout<<"delta_third_Eta= \n "<<delta_third_Eta<<endl;
      //_h_delta_third_Eta->fill(delta_third_Eta,weight);
    
      //Obtaining Phi for bosons and then calculating Delta_Phi //Star#3
      //Calculating various phi between the 3 bosons and obtaining their individual Phi as ph1,phi2, phi3 respectively.Then obtain differences between them which will be the           angle between the 3 bosons.  
      //first_phi is the angle between leptonically decaying W and Z, second_phi is the angle between the two W, third phi is the angle between hadronically decaying W and Z.  
      double phi1 = ( Wboson).phi();
      //cout<<"Wboson_Phi= \n "<<phi1<<endl;
      // _h_Wboson_Phi->fill(phi1,weight);
      double phi2 = ( Zboson).phi();
      //cout<<"Zboson_Phi= \n "<<phi2<<endl;
      // _h_Zboson_Phi->fill(phi2,weight);       
      double phi3 = ( Hadronic_W).phi();
      //cout<<"Hadronic_W_Phi= \n "<<phi3<<endl;
      //_h_Hadronic_W_Phi->fill(phi3,weight);      
      //Calculating delta_Phi between leptonic_W and Z  
      // double delta_first_Phi   = (Wboson + Zboson).deltaPhi();     
      // double delta_first_Phi_old  = phi1 - phi2;
      double delta_first_Phi  = Pi - abs(abs( phi1 - phi2) - Pi);
      //cout<<"delta_first_Phi_corrected  = "<<delta_first_Phi_corrected<<endl;
      // cout<<"delta_first_Phi =  \n"<<delta_first_Phi<<endl;
      // _h_delta_first_Phi->fill(delta_first_Phi,weight);

      //Calculating delta_Phi between Hadronic_W and Leptonic_W
      // double delta_second_Phi  = (Wboson + Hadronic_W).deltaPhi();
      // double delta_second_Phi = phi3 - phi1;
      double delta_second_Phi  = Pi - abs(abs( phi3 - phi1) - Pi);
      // cout<<"delta_second_Phi= \n"<<delta_second_Phi<<endl;
      //_h_delta_second_Phi->fill(delta_second_Phi,weight);
    
      //Calculating delta_Phi between Hadronic_W and Z 
      //double delta_third_Phi  = (Zboson + Hadronic_W).deltaPhi();
      //  double delta_third_Phi = phi3 - phi2;
      double delta_third_Phi  = Pi - abs(abs( phi3 - phi2) - Pi);  
      // cout<<"delta_third_Phi=  \n"<<delta_third_Phi<<endl;
      //_h_delta_third_Phi->fill(delta_third_Phi,weight);

      //Can write deltaR here by formula , but sebastian suggested to use a method from RIVET for calculating delta eta and delta R.
      //Note: This is the delta_R for the three bosons!
      double delta_R_first = sqrt(  (delta_first_Phi*delta_first_Phi) + (delta_first_Eta*delta_first_Eta) );    
      //cout<<"delta_R_first = \n"<<delta_R_first<<endl;
      // _h_delta_R_first->fill(delta_R_first,weight);
      //_h_delta_R_first->fill(delta_R_first>6.9?6.9:delta_R_first,weight); 
      double delta_R_second = sqrt(  (delta_second_Phi*delta_second_Phi) + (delta_second_Eta*delta_second_Eta) );
      //cout<<"delta_R_second = \n "<<delta_R_second<<endl;
      // _h_delta_R_second->fill(delta_R_second,weight);
      // _h_delta_R_second->fill(delta_R_second>6.9?6.9:delta_R_second,weight);
      double delta_R_third = sqrt(  (delta_third_Phi*delta_third_Phi) + (delta_third_Eta*delta_third_Eta) );
      //cout<<"delta_R_third = \n "<<delta_R_third<<endl;
      // _h_delta_R_third->fill(delta_R_third,weight);
      // _h_delta_R_third->fill(delta_R_third>6.9?6.9:delta_R_third,weight);
      //Its stupid to do these variables for WZ system in total. Can use WZ_pt here to get H_t by removing the neutrino component.Can also get the x and y component of this pt. 
      //Note: Need to multiply by       GeV to get the right values !Need to calculate the ET_missing! Check if rivet has a method for that.
      //Obtain Values for WZ system in total ##starC
      double H_t = Zlepton_pt + Wlepton_pt;
      //cout<<"H_t =  \n"<<H_t<<endl;
      //cout<<"###########Lep Loop ends###################: "<<endl; 
      //############Lepton Analysis End###############################################################################

      //##############Histograms Getting Filled Here######################################################################################
      _h_singlejet_m->fill(singlejet_m>139?139:singlejet_m,weight);//overflow for singlejet_m 
      _h_DiJetMass1->fill(DiJetMass1,weight);                                                                                                                                          //_h_DiJetMass2->fill(DiJetMass2,weight);
      _h_ReconMass_BestPair->fill(ReconMass_BestPair<41?41:ReconMass_BestPair,weight);//Underflow for <41                                                                    
      // _h_ReconMass_BestPair->fill(ReconMass_BestPair,weight);//Overflow for >139                                                                
      //_h_ReconMass_NonBestPair->fill(ReconMass_NonBestPair<41?41:ReconMass_NonBestPair,weight);//Underflow for <41                                                     
      //_h_ReconMass_NonBestPair->fill(ReconMass_NonBestPair,weight);//Overflow for >139                                                     
      _h_DiJet_pt_BestPair->fill(DiJet_pt_BestPair>999?999:DiJet_pt_BestPair, weight);//pT for W candidate                                                        
      // _h_DiJet_pt_NonBestPair->fill(DiJet_pt_NonBestPair, weight);//pT for W candidate 
      _h_delta_eta_BestPair->fill(delta_eta_BestPair,weight);
      // _h_delta_eta_NonBestPair->fill(delta_eta_NonBestPair,weight);
      _h_delta_phi_BestPair->fill(delta_phi_BestPair,weight);
      //_h_delta_phi_NonBestPair->fill(delta_phi_NonBestPair,weight);
      _h_Wlepton_pt->fill(Wlepton_pt>999?999:Wlepton_pt, weight);//overflow for Lepton from Leptonic_W Pt
      _h_Zlepton_pt->fill(Zlepton_pt>999?999:Zlepton_pt, weight);// Making a single entry per event.                                                                                                      
      // _h_Zlepton_pt->fill(Zlepton1_pt>999?999:Zlepton1_pt, weight);//StarB //overflow for Leptons from Z. Making a double entries per event.                                                            
      // _h_Zlepton_pt->fill(Zlepton1_pt>200?200:Zlepton1_pt, weight);                                                                                                                                     
      // _h_Zlepton_pt->fill(Zlepton2_pt>999?999:Zlepton2_pt, weight);                                                                                                                                  
      // _h_Zlepton_pt->fill(Zlepton2_pt>200?200:Zlepton2_pt, weight); 
      _h_delta_Eta_lplm->fill(delta_Eta_lplm,weight);
      _h_delta_Phi_lplm->fill(delta_Phi_lplm,weight);
      _h_Wboson_m->fill(Wboson_m<41?41:Wboson_m, weight);//underflow for Invariant Mass for Leptonic_W                                                                          
      _h_Wboson_m->fill(Wboson_m>139?139:Wboson_m, weight);//overflow for Invariant Mass for Leptonic_W                                                                        
      _h_Zboson_m->fill(Zboson_m<41?41:Zboson_m, weight);//underflow for Invariant Mass for Z                                                                                  
      _h_Zboson_m->fill(Zboson_m>139?139:Zboson_m, weight);//overflow for  Invariant Mass for Z    
      _h_Wboson_pt->fill(Wboson_pt>999?999:Wboson_pt, weight); //Overflow for pT Leptonic_W                                                                                     
      _h_Zboson_pt->fill(Zboson_pt>999?999:Zboson_pt, weight);//Overflow for pT of Z  
      _h_Wboson_eta->fill(eta1,weight);
      _h_Zboson_eta->fill(eta2,weight);
      _h_Hadronic_W_eta->fill(eta3,weight);
      _h_delta_first_Eta->fill(delta_first_Eta,weight);
      _h_delta_second_Eta->fill(delta_second_Eta,weight);
      _h_delta_third_Eta->fill(delta_third_Eta,weight);
      _h_Wboson_Phi->fill(phi1,weight);
      _h_Zboson_Phi->fill(phi2,weight);
      _h_Hadronic_W_Phi->fill(phi3,weight); 
      _h_delta_first_Phi->fill(delta_first_Phi,weight);
      _h_delta_second_Phi->fill(delta_second_Phi,weight);
      _h_delta_third_Phi->fill(delta_third_Phi,weight);
      _h_delta_R_first->fill(delta_R_first>6.9?6.9:delta_R_first,weight);
      _h_delta_R_second->fill(delta_R_second>6.9?6.9:delta_R_second,weight);
      _h_delta_R_third->fill(delta_R_third>6.9?6.9:delta_R_third,weight);
      _h_H_t->fill(H_t>999?999:H_t, weight);
      //cout<<"counter="<<counter<<endl;
      //cout<<"jet_counter="<<jet_counter<<endl;
      //cout<<"lep_counter="<<lep_counter<<endl;
      // _h_Ntotalevents->fill(1,counter);
      // _h_Ntotalevents->SetBinContent(2,jet_counter);
      // _h_Ntotalevents->SetBinContent(3,lep_counter);
    }

    void finalize() {

      /// @todo Normalise, scale and otherwise manipulate histograms here
      //    scale(_h_2l2j, crossSection()/sumOfWeights()/femtobarn); // norm to cross section
    }

    //@}
    //  private:
  public: 
   
    //Defining 1D histograms for Z
    // Histo1DPtr _h_eee;
    // Histo1DPtr _h_mee;       
    // Histo1DPtr _h_emm; 
    // Histo1DPtr _h_mmm;   
    // Histo1DPtr _h_fid;     
    // Histo1DPtr _h_eee_Plus  ;
    // Histo1DPtr _h_mee_Plus  ;
    // Histo1DPtr _h_emm_Plus  ; 
    // Histo1DPtr _h_mmm_Plus  ; 
    // Histo1DPtr _h_fid_Plus  ; 
    // Histo1DPtr _h_eee_Minus ; 
    // Histo1DPtr _h_mee_Minus ; 
    // Histo1DPtr _h_emm_Minus; 
    // Histo1DPtr _h_mmm_Minus ; 
    // Histo1DPtr _h_fid_Minus ; 
    // Histo1DPtr _h_Deltay_Plus;
    // Histo1DPtr _h_Deltay_Plus_norm;
    // Histo1DPtr _h_Deltay_Minus;
    // Histo1DPtr _h_Deltay_Minus_norm;   
    // Histo1DPtr _h_Deltay;
    // Histo1DPtr _h_Deltay_norm;
    Histo1DPtr _h_Wboson_m;
    Histo1DPtr _h_Zboson_m;
    Histo1DPtr _h_Wboson_mT;
    Histo1DPtr _h_nu_pT;
    Histo1DPtr _h_Wboson_pt;
    Histo1DPtr _h_Zboson_pt;
    Histo1DPtr _h_Wboson_eta;
    Histo1DPtr _h_Zboson_eta;
    Histo1DPtr _h_weights;
    Histo1DPtr _h_H_t;
    Histo1DPtr _h_delta_Eta_lplm;
    Histo1DPtr _h_delta_Phi_lplm;
    Histo1DPtr _h_DiJetMass1;
    Histo1DPtr _h_DiJetMass2;
    Histo1DPtr _h_singlejet_m;
    Histo1DPtr _h_DiJet_pt_BestPair;
    Histo1DPtr _h_DiJet_pt_NonBestPair;
    Histo1DPtr _h_Njets;
    Histo1DPtr _h_delta_eta_BestPair;
    Histo1DPtr _h_delta_eta_NonBestPair;
    Histo1DPtr _h_delta_phi_BestPair;
    Histo1DPtr _h_delta_phi_NonBestPair;
    Histo1DPtr _h_ReconMass_BestPair;
    Histo1DPtr _h_ReconMass_NonBestPair;
    Histo1DPtr _h_Ntotalevents;
    Histo1DPtr _h_Wlepton_pt;
    Histo1DPtr _h_Zlepton_pt;
    //Histo1DPtr _h_theta_ll;
    Histo1DPtr _h_Wboson_Phi;
    Histo1DPtr _h_Hadronic_W_Phi;
    Histo1DPtr _h_Zboson_Phi;
    Histo1DPtr _h_delta_first_Eta;  
    Histo1DPtr _h_delta_second_Eta;
    Histo1DPtr _h_delta_third_Eta;
    Histo1DPtr _h_delta_first_Phi;
    Histo1DPtr _h_delta_second_Phi;
    Histo1DPtr _h_delta_third_Phi;
    Histo1DPtr _h_delta_R_first;
    Histo1DPtr _h_delta_R_second;
    Histo1DPtr _h_delta_R_third;
    Histo1DPtr  _h_Hadronic_W_eta;
    Histo1DPtr _h_Nlepevents_after;
    //  Histo1DPtr _h_Nlepevents_before;
    Histo1DPtr _h_Njetevents;
    Histo1DPtr _h_Nlep_before;
    Histo1DPtr _h_Nlep_after;
    Histo1DPtr _h_Nnu;
    double MZ_PDG = 91.1876;
    double MW_PDG = 80.385;
    double Pi = 3.14159265;
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(WWZ);
}
