Setup environment
------
Setup ATLAS and root::

  . plotter_setup.sh

Run plotting tool, e.g.::

  python run.py listRootFiles.txt WWZ//HT >& log &
  tail -f log

where the python option ``LogY``, if set to true, allows to plot data/MC with log scale on the y axis.
