from MadGraphControl.MadGraphUtils import *
nevents=1000
#nevents = 10000                                                                                                                                                                                            
mode=0
### DSID lists (extensions can include systematics samples)                                                                                                                                                 
test=[999999]
fcard = open('proc_card_mg5.dat','w')
if runArgs.runNumber in test:

    fcard.write("""                                                                                                                                                                                        
#    import model SM_LT012_UFO                                                                                                                                                                             #import model /afs/cern.ch/user/n/ngubbi/UFO-Models-aQGC/SM_LM0 # This is for running lxplus on Bonn Grid
#No need to specify the path of UFO MODEL for grid jobs. If the athena version and MG version is the latest then the job picks up the model from cvmfs. Currently using the athena 19.2.5.35 
  
 
    import model SM_LT0                                                                                                                                                                                      
    define p = g u c d s b u~ c~ d~ s~ b~                                                                                                                                                                       
                                                                                                                                                                                                           
    define j = g u c d s b u~ c~ d~ s~ b~                                                                                                                                                                      
                                                                                                                                                                                                           
    generate p p > W- W+ Z QCD=0 QED=3 NP=1                                                                                                                                                                    #the QCD,QED,NP are the couplings to be associated with the process; this was the crucial part , without which it was generating only few aQGC diagrams and didn't include SM contribution at all. 
    #BTW, NP is New Physics!!                                                                                                                                                                                                      
    output -f""")

    fcard.close()
#import model SM_LM0123

else:

    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

evt_multiplier = 4

if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevents=runArgs.maxEvents*evt_multiplier
    else:
        nevents=5000*evt_multiplier

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process()
#Fetch default NLO run_card.dat and set parameters 
extras = { 'lhaid'         :"262000",

           'pdlabel'       :"'lhapdf'",

           'parton_shower' :'PYTHIA8',

           'bwcutoff'      :40,

           'reweight_scale':'.false.'}

#build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='new_run_card.dat', nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
#build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='new_run_card.dat', nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

madspin_card_loc='madspin_card.dat'

if madspin_card_loc:
    mscard = open(madspin_card_loc,'w')
    mscard.write("""#************************************************************                                                                                                                          
                                                                                                                                                                                                       
#*                        MadSpin                           *                                                                                                                                              
#*                                                          *                                                                                                                                              
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *                                                                                                                                              
#*                                                          *                                                                                                                                              
#*    Part of the MadGraph5_aMC@NLO Framework:              *                                                                                                                                              
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *                                                                                                                                              
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *                                                                                                                                              
#*                                                          *                                                                                                                                              
#************************************************************ 
#Some options (uncomment to apply)                                                                                                                                                                         
#                                                                                                                                                                                                          
# set seed 1                                                                                                                                                                                                
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight                                                                                                                      
set BW_cut 40                # cut on how far the particle can be off-shell                                                                                                                                 
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event                                                                                                                         
#                                                                                                                                                                                                       
set seed %i                                                                                                                                                                                                                                                                                                                                                                                                        
# specify the decay for the final state particles                                                                                                                                                          
                                                                                                                                                                                                          
define lp = e+ mu+ ta+                                                                                                                                                                                     
                                                                                                                                                                                                           
define lm = e- mu- ta-                                                                                                                                                                                     
                                                                                                                                                                                                          
#define j = g u c d s u~ c~ d~ s~                                                                                                                                                                          
                                                                                                                                                                                                          
#define vl = ve vm vt                                                                                                                                                                                      
                                                                                                                                                                                                         
#define vl~ = ve~ vm~ vt~             

decay t > w+ b, w+ > all all                                                                                                                                                                                                                                                                                                                                                                                        
decay t~ > w- b~, w- > all all                                                                                                                                                                                                                                                                                                                                                                                      
#decay w+ > lp vl                                                                                                                                                                                                                                                                                                                                                                                                   
#decay w+ > all all                                                                                                                                                                                                                                                                                                                                                                                                 
#decay w- > lm vl~                                                                                                                                                                                                                                                                                                                                                                                                 
#decay w- > all all                                                                                                                                                                                                                                                                                                                                                                                                
#decay z > all all                                                                                                                                                                                         
#decay z > lp lm     

#4l2v final state                                                                                                                                                                                          
#decay w+ > lp vl                                                                                                                                                                                          
#decay w- > lm vl~                                                                                                                                                                                         
#decay z > lp lm                                                                                                                                                                                           
                                                                                                                                                                                                          
#3l1v2j final state                                                                                                                                                                                        
decay w+ > all all                                                                                                                                                                                         
decay w- > all all                                                                                                                                                                                         
decay z > lp lm                                                                                                                                                                                       

launch"""%runArgs.randomSeed)
mscard.close()
print_cards()
runName='run_01_aQGC-3lepevents'
#generate(run_card_loc='new_run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir,run_name=runName,madspin_card_loc=madspin_card_loc)
generate(run_card_loc='new_run_card.dat',param_card_loc='param_card_T0.dat',mode=mode,proc_dir=process_dir,run_name=runName,madspin_card_loc=madspin_card_loc)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._001aQGC.events.tar.gz',lhe_version=3,saveProcDir=True)

#### Shower                                                                                                                                                                                                

#This is I think for NLO showering 
#evgenConfig.description = 'aMcAtNlo_WWW_NLO'
#evgenConfig.keywords+=['Z','electron','jets','drellYan']
#evgenConfig.inputfilecheck = runName
#runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
#include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
#include("MC15JobOptions/Pythia8_aMcAtNlo.py")
#include("MC15JobOptions/Pythia8_MadGraph.py")

#May be I need to make this change for LO showering
evgenConfig.description = 'aMcAtLo_WWZ_LO'
evgenConfig.keywords+=['Z','electron','jets','drellYan']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")                                                                                                               
include("MC15JobOptions/Pythia8_MadGraph.py") 
##---------All filter commented out-----------------------------------------##                                                                                                                                                     
evgenLog.info('3Lep6GeV filter is applied')
include ( 'MC15JobOptions/DecaysFinalStateFilter.py' )
filtSeq.DecaysFinalStateFilter.PDGAllowedParents = [ 23, 24, -24 ]
filtSeq.DecaysFinalStateFilter.NChargedLeptons = 3   

#include ( 'MC15JobOptions/MultiElecMuTauFilter.py' )                                                                                                                                                      
#filtSeq.MultiElecMuTauFilter.MinPt  = 6000.                                                                                                                                                               
#filtseq.MultiElecMutauFilter.PDGAllowedParents = [ Z, W+ , W- ]                                                                                                                                           
#filtSeq.MultiElecMuTauFilter.MaxEta = 2.8                                                                                                                                                                 
#filtSeq.MultiElecMuTauFilter.NLeptons = 3                                                                                                                                                                 
#filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0                                                                                                                                                           
#filtSeq.Expression = "MultiElecMuTauFilter"                                                                                                                                                                                                                                                                                                                                  
                                                                                           

