#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"

namespace Rivet {


  class WWZ : public Analysis {
  public:

    /// Constructor
    WWZ() : Analysis("WWZ") {    }

    /// @name Analysis methods
    /// Book histograms and initialise projections before the run
    void init() {
   
      //****************init() FOR W->jj*************************************
      //Charged Leptons within acceptance-for W ; Neutrinos,b-hadrons not included in FS
      Cut fs_z = Cuts::abseta < 5 && Cuts::pT >0.1*GeV; //Loose Cuts
      // Cut fs_z = Cuts::abseta <2.5 && Cuts::pT >15*GeV;

      // const PromptFinalState chLep_fid = PromptFinalState(fs_z && ( Cuts::abspid == PID::ELECTRON || Cuts::abspid == PID::MUON ));
      const PromptFinalState chLep_fid = PromptFinalState(fs_z && ( Cuts::abspid == PID::ELECTRON || Cuts::abspid == PID::MUON ), false, false );
      const PromptFinalState photon_fs = PromptFinalState ( Cuts::abspid == PID::PHOTON);// To get Photons to dress leptons 
      const DressedLeptons dressed_leps(photon_fs,chLep_fid,0.1,Cuts::pT>0.1*GeV);
      declare(dressed_leps,"Dressed Leptons");
      //Jets,ANti-kt 0.4
      VetoedFinalState fsJets(FinalState(Cuts::abseta<7.0));//final state for jet finding: veto leptons and neutrinos
      fsJets.vetoNeutrinos();
      fsJets.addVetoOnThisFinalState(chLep_fid);
      declare(FastJets(fsJets,FastJets::ANTIKT,0.4),"Jets");
      
      //****************init() FOR WZ*************************************
      const FinalState fs;
                                                                                                                 
      //Lepton Cuts
      //Cut FS_Zlept = Cuts::abseta < 2.5 && Cuts::pT > 15*GeV;
      Cut FS_Zlept = Cuts::abseta < 5 && Cuts::pT > 0.1*GeV;//Loose Cuts
      //Cut fs_z = Cuts::abseta < 2.5 && Cuts::pT > 15*GeV;
      Cut fs_j = Cuts::abseta < 5 && Cuts::pT > 2.5*GeV;
      
      //Get photons to dress leptons-for Z                                                                                                                                                                 
      PromptFinalState photons(Cuts::abspid == PID::PHOTON);

      //Electrons and muons in Fiducial PS- for Z;Total PS exluded for now                                                                                                                      
      //PromptFinalState leptons (Cuts::abspid == PID::ELECTRON || Cuts::abspid == PID::MUON);
      PromptFinalState leptons( fs_z && (Cuts::abspid == PID::ELECTRON || Cuts::abspid == PID::MUON), true, false );
      // PromptFinalState leptons( Cuts::abspid == PID::ELECTRON || Cuts::abspid == PID::MUON,bool accepttaudecays=false, bool acceptmudecays=false ); 
      leptons.acceptTauDecays(true);
      DressedLeptons dressedleptons(photons, leptons, 0.1, FS_Zlept, true);
      addProjection(dressedleptons, "DressedLeptons");
      //Neutrinos
      IdentifiedFinalState nu_id;
      nu_id.acceptNeutrinos();
      PromptFinalState neutrinos(nu_id);
      // neutrinos.acceptTauDecays(false);
      neutrinos.acceptTauDecays(true);
      declare(neutrinos, "Neutrinos");
      MSG_WARNING("LIMITED VALIDITY - check info file for details!\033[m");
      //Jets-For Z
      VetoedFinalState veto;
      veto.addVetoOnThisFinalState(dressedleptons);
      FastJets jets(veto, FastJets::ANTIKT, 0.4);
      declare(jets, "JetsZ");


      /// Book histograms
   
      // For W -> jj
      _h_Wjj_Njets  = bookHisto1D("Wjj_Njets"    ,12  ,  0,  12, "Number of Jets"                  , "N_jets"               , "(1/N) n. of events");
      _h_Wjj_m      = bookHisto1D("Wjj_m"        ,40  , 70, 100, "Invariant Mass from any Jet pair", "Invariant Wmass [GeV]", "(1/N) n. of events");     
      _h_singlejet_m= bookHisto1D("singlejet_m"  ,40  , 70, 100, "Invariant mass from single Jet"  , "singlejet_mass [GeV]" , "(1/N) n. of events"); 
      _h_Wjj_pt     = bookHisto1D("Wjj_pt"       ,2000,  0, 1000,"Jets pT"                         , "pT from jets"         , "(1/N) n. of events");
      _h_Wjj_eta    = bookHisto1D("Wjj_eta"      ,12  ,  0, 3.14, "eta from jets"                  , "Eta from Jets"        ,       "(1/N) n. of events");
      _h_Wjj_m_best = bookHisto1D("Wjj_m_best"   ,40  , 70, 100, "Invariant W Mass from best pair of Jets", "Invariant Wmass[GeV]","(1/N) n. of events");
      //Initialising histograms for WZ->3l,1v 
      //_h_eee        = bookHisto1D("eee"		,100, 0, 100, "eee", "eee",   "(1/N) n. of events");
      //_h_mee        = bookHisto1D("mee"		,100, 0, 100, "mee", "mee",   "(1/N) n. of events");
      // _h_emm        = bookHisto1D("emm"		,100, 0, 100, "emm", "emm",   "(1/N) n. of events");
      // _h_mmm        = bookHisto1D("mmm"		,100, 0, 100, "mmm", "mmm",   "(1/N) n. of events");
      // _h_fid        = bookHisto1D("fid"		,100, 0, 100, "n. of events", "fid",       "(1/N) n. of events");
      // _h_eee_Plus   = bookHisto1D("eee_Plus"	,100, 0, 100, "n. of events", "eee_Plus",  "(1/N) n. of events");
      // _h_mee_Plus   = bookHisto1D("mee_Plus"	,100, 0, 100, "n. of events", "mee_Plus",  "(1/N) n. of events");
      // _h_emm_Plus   = bookHisto1D("emm_Plus"	,100, 0, 100, "n. of events", "emm_Plus",  "(1/N) n. of events");
      // _h_mmm_Plus   = bookHisto1D("mmm_Plus"	,100, 0, 100, "n. of events", "mmm_Plus",  "(1/N) n. of events");
      // _h_fid_Plus   = bookHisto1D("fid_Plus"	,100, 0, 100, "n. of events", "fid_Plus",  "(1/N) n. of events");
      // _h_eee_Minus  = bookHisto1D("eee_Minus"	,100, 0, 100, "n. of events", "eee_Minus", "(1/N) n. of events");
      // _h_mee_Minus  = bookHisto1D("mee_Minus"	,100, 0, 100, "n. of events", "mee_Minus", "(1/N) n. of events");
      // _h_emm_Minus  = bookHisto1D("emm_Minus"	,100, 0, 100, "n. of events", "emm_Minus", "(1/N) n. of events");
      // _h_mmm_Minus  = bookHisto1D("mmm_Minus"	,100, 0, 100, "n. of events", "mmm_Minus", "(1/N) n. of events");
      // _h_fid_Minus  = bookHisto1D("fid_Minus"	,100, 0, 100, "n. of events", "fid_Minus", "(1/N) n. of events");
 
      _h_Wboson_m   = bookHisto1D("Wboson_m"    ,40, 70, 100, "Invariant W Mass","W mass [GeV]","(1/N) n. of events");
      _h_Zboson_m   = bookHisto1D("Zboson_m"    ,40, 70, 100, "Invariant Z Mass","Z mass [GeV]","(1/N) n. of events");
      // _h_Wboson_mT  = bookHisto1D("Wboson_mT"   ,200, 0, 200, "Transverse W Mass","W Transverse mass [GeV]","(1/N) n. of events");
      // _h_Zboson_mT  = bookHisto1D("Zboson_mT"   ,100, 0, 500, "n. of events","Zboson Transverse mass","(1/N) n. of events");     
   
      _h_Wboson_pt  = bookHisto1D("Wboson_pt"   ,2000, 0, 1000, "Wboson pT","Lep_W_pT [GeV]","(1/N) n. of events");
      _h_Zboson_pt  = bookHisto1D("Zboson_pt"   ,2000, 0, 1000, "Zboson pT","Z_pT [GeV]"    ,"(1/N) n. of events");
     
      _h_Wboson_eta = bookHisto1D("Wboson_eta"          ,10 ,0, 3.14, "eta for Lep_W"    ,"Lep_W_eta","(1/N) n. of events");
      _h_Zboson_eta = bookHisto1D("Zboson_eta"          ,10 ,0, 3.14, "eta for Z"        ,"Z_eta"    ,"(1/N) n. of events");
      _h_Hadronic_W_eta = bookHisto1D("Hadronic_W_eta"  ,10 ,0, 3.14, "eta for Had_W"    ,"Had_W_eta","(1/N) n. of events");
    
      _h_Wboson_Phi = bookHisto1D("Wboson_Phi"          ,10 ,0, 3.14, "Phi for Lep_W","Lep_W_Phi","(1/N) n. of events");
      _h_Hadronic_W_Phi = bookHisto1D("Hadronic_W_Phi"  ,10 ,0, 3.14, "Phi for Z"    ,"Had_W_Phi","(1/N) n. of events");
      _h_Zboson_Phi = bookHisto1D("Zboson_Phi"          ,10 ,0, 3.14, "Phi for Had_W","Z_Phi"    ,"(1/N) n. of events");

      _h_H_t      = bookHisto1D("H_t"           ,2000, 0, 1000, "H_t [GeV]","WZ Transverse Momentum","(1/N) n. of events");

      _h_theta_ll      = bookHisto1D("theta_ll"   ,20, 0, 3.14, "Angle theta between l+ and l-","theta l+l-","(1/N) n. of events");
      // _h_mTWZ       = bookHisto1D("_mTWZ"       ,300, 0, 300, "n. of events","WZ Transverse mass","(1/N) n. of events");
      _h_Ntotalevents = bookHisto1D("Ntotalevents",1,99,101,"total n. of events","Nevents without any selections","(1/N) n. of events");
 
    
      _h_Wlepton_pt = bookHisto1D("Wlepton_pt", 2000, 0, 1000, "pT for lepton from W","pT [GeV]","(1/N) n. of events");
      _h_Zlepton_pt = bookHisto1D("Zlepton_pt", 2000, 0, 1000, "pT for lepton from Z","pT [GeV]","(1/N) n. of events");
      _h_delta_Eta_lplm  = bookHisto1D("delta_Eta_lplm"    ,10 ,0, 3.14, "Delta Eta between l+l-","Delta Eta l+l-","(1/N) n. of events");
      _h_delta_Phi_lplm =bookHisto1D("delta_Phi_lplm"      ,10 ,0, 3.14, "Delta Phi between l+l-","Delta Phi l+l-","(1/N) n. of events");
      _h_delta_first_Eta  = bookHisto1D("delta_first_Eta"  ,10 ,0, 3.14, "Delta Eta between Leptonic_W and Z"         ,"Delta Eta","(1/N) n. of events");
      _h_delta_second_Eta  = bookHisto1D("delta_second_Eta",10 ,0, 3.14, "Delta Eta between Leptonic_W and Hadronic_W","Delta Eta","(1/N) n. of events");
      _h_delta_third_Eta  = bookHisto1D("delta_third_Eta"  ,10 ,0, 3.14, "Delta Eta between Hadronic_W and Z"         ,"Delta Eta","(1/N) n. of events");
    
      _h_delta_first_Phi  = bookHisto1D("delta_first_Phi"  ,10,0,3.14, "Delta Phi between Leptonic W and Z"         ,"delta Phi","(1/N) n. of events");
      _h_delta_second_Phi  = bookHisto1D("delta_second_Phi",10,0,3.14, "Delta Phi between Leptonic_W and Hadronic_W","delta Phi","(1/N) n. of events");
      _h_delta_third_Phi  = bookHisto1D("delta_third_Phi"  ,10,0,3.14, "Delta Phi between Hadronic_W and Z"         ,"delta Phi","(1/N) n. of events");

      _h_delta_R_first =bookHisto1D("delta_R_first  "      ,10,0,7, "Delta R between Leptonic_W and Z"         ,"delta R","(1/N) n. of events" );
      _h_delta_R_second =bookHisto1D("delta_R_second"      ,10,0,7, "Delta R between Leptonic_W and Hadronic_W","delta R","(1/N) n. of events" );
      _h_delta_R_third =bookHisto1D("delta_R_third  "      ,10,0,7, "Delta R between Hadronic_W and Z"         ,"delta R","(1/N) n. of events" );


      _h_Nlepevents_after = bookHisto1D("Nlepevents_after" ,1 ,0 ,2,"n. of events","Nevents after lepton selections","(1/N) n. of events");
      _h_Njetevents =bookHisto1D("Njetevents"              ,1 ,0 ,2 ,"n. of events","Nevents of jet","(1/N) n. of events");
      _h_Nlep_before= bookHisto1D("Nlep_before"            ,5 ,0 ,5,      "n. of events","Number of Leptons before selection","(1/N) n. of events");
      _h_Nlep_after = bookHisto1D("Nlep_after"             ,5 ,0 ,5,      "n. of events","Number of Leptons after selection","(1/N) n. of events");
      _h_Nnu        = bookHisto1D("Nnu"                    ,15,0 ,15,    "Number of Neutrinos","N_nu","(1/N) n. of events");

    }
    /// Perform the per-event analysis
    void analyze(const Event& event) {
    
      //
      /// @todo Do the event by event analysis here
      const double weight = event.weight();
      FourMomentum Wjet1,Wjet2,Hadronic_W,Zlepton1,Zlepton2,Wlepton;
      //Histogram with No selections, just giving total number of events
      _h_Ntotalevents->fill(100.0,1);

      // ****************analyse() FOR W->jj*************************************
      //Get Leptons
      vector<DressedLepton> leps = apply<DressedLeptons>(event,"DressedLeptons").dressedLeptons();
      // if(leps.size() <2 )
      //	vetoEvent;
      // if (leps.size() < 2) vetoEvent;
                                                                                                                                                          
      //Sort the dressed leptons by pt of their constituent lepton (bare lepton pt)
      // std::sort(leps.begin(), leps.end() ,[](const DressedLepton& l1, const DressedLepton& l2)
      //	{
      //	  return (l1.constituentLepton().pT() > l2.constituentLepton().pT());
      //	}
      //	);
      //Removing these cuts to make them as loose as possible
      // if (leps[0].pT() < 30*GeV || leps[0].abseta() > 2.5)  vetoEvent;
      // if (leps[1].pT() < 30*GeV || leps[1].abseta() > 2.5)  vetoEvent;

      //Get Jets
      const Jets& jets = apply<FastJets>(event,"Jets").jetsByPt(Cuts::pT>15*GeV);
     
      //Jets analysis
      double Njets = jets.size();
      double bestW_mjj(-1),jj_pt(-1),jj_eta(-1);
      // FourMomentum Wjet1,Wjet2; 

      //Check for Njets<2. If its 0 jets , display error message but if its >0 and ==1, then obtain the Invariant mass of the single jet; Also, fill Wjj_Njets Histo to count for number of jets so that events with one jet is also included.
      if(Njets<2){
	double singlejet_m = (Njets==0) ? -99. : (jets[0].momentum()).mass();
	//cout<<"singlejet Invariant mass= \n"<<singlejet_m<<endl;
	//_h_singlejet_m->fill(singlejet_m>100?100:singlejet_m,weight);
	_h_singlejet_m->fill(singlejet_m>99?99:singlejet_m,weight);//overflow for singlejet_m
       	//cout<<"less than two jets"<<endl;
	_h_Wjj_Njets-> fill(Njets,weight);	
	vetoEvent;
      }

      if(Njets>=2){
	//cout<<"Number of Jets= \n"<<Njets<<endl;
       	_h_Njetevents->fill(1.0,1);//To obtain the number of events after jet selections of having atleast two jets
	_h_Wjj_Njets-> fill(Njets,weight);//To obtain number of jets

	//Loop over a given pair of jets; Obtain invariant mass from them and fill into respective histogram
	for( int a =0;a<Njets;a++) {
	  for(int b =0;b<Njets;b++) {
	    if (a>=b) continue;
	    double mjj = (jets[a].momentum() + jets[b].momentum()).mass();//
	    //cout<<"InvariantMass mjj= "<<mjj<<endl;
	    //  if (mjj < 65 || mjj > 105)  vetoEvent;
	    _h_Wjj_m->fill(mjj,weight);

	    //Among the various pairs of jets, Select the best jj pair that gives the Invariant mass closest to the Wmass 
	    bestW_mjj = (bestW_mjj==-1) ? mjj : bestW_mjj;
	    //    cout<<"bestW_mjj= \n"<<bestW_mjj<<endl; 
	    bestW_mjj = (abs(bestW_mjj-MW_PDG)>abs(mjj-MW_PDG)) ? mjj : bestW_mjj;
	    // cout<<"mjj= "<<mjj<<endl;
	    // cout<<"bestW_mjj= "<<bestW_mjj<<endl;

	    FourMomentum jet0 = jets[a].momentum();
	    // cout<<"jet0 = "<<jet0<<endl;
	    FourMomentum jet1 = jets[b].momentum();
	    // cout<<"jet1 = "<<jet1<<endl;
	    FourMomentum jj = jets[a].momentum() + jets[b].momentum();
	   
	    //Need to select two jets with Four momentum that gives the bestW_mjj;From these jets four momentum, can obtain eta,phi,pt
	    if( ((jets[a].momentum() + jets[b].momentum()).mass() )==bestW_mjj ){
	      Wjet1 =  jets[a].momentum();
	      Wjet2 =  jets[b].momentum();
	    }
	  }
	}
	//Obtain eta,phi,pt and fill into histos
	jj_pt = (Wjet1 + Wjet2).pT()*GeV;                                                                                                                                     
	jj_eta = (Wjet1 + Wjet2).eta();

	//jj_phi = (Wjet1 + Wjet2).phi();//can add.phi() also in here
	_h_Wjj_m_best->fill(bestW_mjj<71?71:bestW_mjj,weight);//Underflow for <70
	_h_Wjj_m_best->fill(bestW_mjj>95?95:bestW_mjj,weight);//Overflow for >95	 
	//_h_Wjj_pt->fill(jj_pt,weight);        
	_h_Wjj_pt->fill(jj_pt>999?999:jj_pt, weight);//pT for W candidate
	_h_Wjj_eta->fill(jj_eta,weight);

      }
      // ****************analysis for WZ*************************************
      //Initialise variables needed for WZ
      int i, j, k;
      double MassZ01 = 0., MassZ02 = 0., MassZ12 = 0.;
      double MassW0 = 0., MassW1 = 0., MassW2 = 0.;
      double WeightZ1, WeightZ2, WeightZ3;
      double WeightW1, WeightW2, WeightW3;
      double M1, M2, M3;
      double WeightTotal1, WeightTotal2, WeightTotal3;
     

      //obtain dressed leptons and neutrinos for WZ system
      const vector<DressedLepton>& dressedleptons = apply<DressedLeptons>(event, "DressedLeptons").dressedLeptons();
      const Particles& neutrinos = apply<PromptFinalState>(event, "Neutrinos").particlesByPt();
      Jets jets_Z = apply<JetAlg>(event, "Jets").jetsByPt( (Cuts::abseta < 5) && (Cuts::pT > 2.5*GeV) );//Do we need this at all, since we arent interested with Jets from Z?

       
      //Fiducial PS: assign leptons to W and Z bosons using Resonant shape algorithm
       int EventType = -1;//
      int Nel = 0, Nmu = 0,Nlep_before=0,Nlep_after=0,Nnu=0;//Number of various leptons and neutrinos
      //Obatin number of neutrinos and fill histo
      Nnu = neutrinos.size();
      //cout<<"Total number of neutrinos Nnu ="<<Nnu<<endl;
      _h_Nnu->fill(Nnu,weight);
     
      //select electrons and muons and count their numbers
      for (const DressedLepton& l : dressedleptons)
	{

	  if (l.abspid() == 11)   //electron 
	    ++Nel;
	  if (l.abspid() == 13)  //Muon
	    ++Nmu;
	  //	if (l.abspid() == 15)  //Tau                                                                                                                                                               
	  // ++Ntau;
	  //cout<<"PDGId of leps are= "<<l.abspid()<<endl;//Display PDG ID to be sure what leptons we are getting
	}
      
      //Determining number of e,mu and nu, tau
      //      cout<<"Number of taus =  "<<Ntau<<endl;
      Nlep_before = Nel+Nmu;//Count number of leptons before making the 3lepton selections and fill histo
      //cout<<"Total number of leptons Nlep before 3lep selection="<<Nlep_before<<endl;
      _h_Nlep_before-> fill(Nlep_before,weight);
      // _h_lepevents_before->fill(100.0,1);    

      //How to find the number of lepton events in various combinations havinga total of 3 leptons between electrons and muons;This is the 3LEPTON SELECTION!
      if      ( Nel == 3  && Nmu==0 )  { EventType = 3;// cout << "Event Type = 3;\n"<<"Number of e = 3,Number of mu = 0"<< endl;
      } 
      else if ( Nel == 2  && Nmu==1 )  { EventType = 2;// cout << "Event Type = 2;\n"<<"Number of e = 2,Number of mu = 1"<< endl;
      } 
      else if ( Nel == 1  && Nmu==2 )  { EventType = 1;// cout << "Event Type = 1;\n"<<"Number of e = 1,Number of mu = 2"<< endl;
      } 
      else if ( Nel == 0  && Nmu==3 )  { EventType = 0;// cout << "Event Type = 0;\n"<<"Number of e = 0,Number of mu = 3"<< endl;
      } 
      else{
	vetoEvent;//Especially important to veto events to discard any events that doesnt have these combinations so that we get correct number of lepton events after passing 3lepton selections
      }
      Nlep_after = Nel+Nmu;//Count number of leptons after making the 3lepton selections and fill histo
      //cout<<"Total number of leptons Nlep after 3lep selection="<<Nlep_after<<endl;       
      _h_Nlep_after-> fill(Nlep_after,weight); 
      _h_Nlepevents_after->fill(1.0,1);//Counts the number of events after making jet selections and 3lep selections;these events are those that have atleast 2jets and 3leps
      
      int EventCharge = -dressedleptons[0].charge()*dressedleptons[1].charge()*dressedleptons[2].charge();
      //cout<<"Event charge = \n"<<EventCharge<<endl;

      //Determining Charge and PID of these leptons                                                                                                                                                         
      // double lepton1_charge = dressedleptons[0].charge();
      // double lepton1_ParticleID = (dressedleptons[0]).pid();
      // cout<<"lepton1_charge =\n"<<lepton1_charge<<endl;
      // cout<<"lepton1_ParticleID =\n"<<lepton1_ParticleID<<endl;

      // double lepton2_charge = (dressedleptons[1]).charge();
      // double lepton2_ParticleID = (dressedleptons[1]).pid();
      // cout<<"lepton2_charge =\n"<<lepton2_charge<<endl;
      // cout<<"lepton2_ParticleID =\n"<<lepton2_ParticleID<<endl;

      // double lepton3_charge = (dressedleptons[2]).charge();
      // double lepton3_ParticleID = (dressedleptons[2]).pid();
      // cout<<"lepton3_charge =\n"<<lepton3_charge<<endl;
      // cout<<"lepton3_ParticleID =\n"<<lepton3_ParticleID<<endl;

      //Took the particle ID of the 3  dressedleptons and in some instaces , lepton1_ParticleID != -(lepton2)_ParticleID......so cannot directly assume tat lepton1 and lepton2 are always originating from      Z. There are 3 cases here and need to this !!
      MassZ01 = 0; MassZ02 = 0; MassZ12 = 0;
      MassW0 = 0;  MassW1 = 0;  MassW2 = 0;

      //Determing the leptons and their origin; Z->l+,l- and W->l,nu
      //try Z pair of leptons 0 and lepton 1
      if ( (dressedleptons[0].pid()==-(dressedleptons[1].pid())) && (dressedleptons[2].abspid()==neutrinos[0].abspid()-1))
	{
	  //cout<<"leptons l0,l1 from Z and "<<"l2 from W "<<endl;
	  MassZ01 = (dressedleptons[0].momentum()+dressedleptons[1].momentum()).mass();
	  //cout<<"MassZ01 = \n"<<MassZ01<<endl;      
	  MassW2 = (dressedleptons[2].momentum()+neutrinos[0].momentum()).mass();
	  //cout<<"MassW2 = \n"<<MassW2<<endl;
	}
      //try Z pair of leptons 0 and lepton 2
      if ( (dressedleptons[0].pid()==-(dressedleptons[2].pid())) && (dressedleptons[1].abspid()==neutrinos[0].abspid()-1))
	{
	  //cout<<"leptons l0,l2 from Z "<<"lepton l1 from W"<<endl;
	  MassZ02 = (dressedleptons[0].momentum()+dressedleptons[2].momentum()).mass();
	  MassW1 = (dressedleptons[1].momentum()+neutrinos[0].momentum()).mass();
	  //cout<<"MassZ02 = \n"<<MassZ02<<endl;
	  //cout<<"MassW1 = \n"<<MassW1<<endl;
	}
      //try Z pair of leptons 1 and lepton 2
      if ( (dressedleptons[1].pid()==-(dressedleptons[2].pid())) && (dressedleptons[0].abspid()==neutrinos[0].abspid()-1))
	{
	  //cout<<"leptons l1,l2 from Z "<<"lepton l0 from W"<<endl;
	  MassZ12 = (dressedleptons[1].momentum()+dressedleptons[2].momentum()).mass();
	  MassW0 = (dressedleptons[0].momentum()+neutrinos[0].momentum()).mass();
	  //cout<<"MassZ12 =  \n"<<MassZ12<<endl;
	  //cout<<"MassW0  =  \n "<<MassW1<<endl;
	}
      //Using Breit Wigner obtain the Mass that is closest to Zmass
      WeightZ1 = 1/(pow(MassZ01*MassZ01 - MZ_PDG*MZ_PDG,2) + pow(MZ_PDG*GammaZ_PDG,2));
      WeightW1 = 1/(pow(MassW2*MassW2 - MW_PDG*MW_PDG,2) + pow(MW_PDG*GammaW_PDG,2));
      //cout<<"WeightZ1= \n "<<WeightZ1<<endl;
      //cout<<"WeightW1= \n "<<WeightW1<<endl;
      WeightTotal1 = WeightZ1*WeightW1;
      M1 = -1*WeightTotal1;
      //cout<<"WeightTotal1= \n"<<WeightTotal1<<endl;
      //cout<<"M1= \n "<<M1<<endl;

      WeightZ2 = 1/(pow(MassZ02*MassZ02- MZ_PDG*MZ_PDG,2) + pow(MZ_PDG*GammaZ_PDG,2));
      WeightW2 = 1/(pow(MassW1*MassW1- MW_PDG*MW_PDG,2) + pow(MW_PDG*GammaW_PDG,2));
      //cout<<"WeightZ2= \n "<<WeightZ2<<endl;
      //cout<<"WeightW2= \n "<<WeightW2<<endl;      
      WeightTotal2 = WeightZ2*WeightW2;
      M2 = -1*WeightTotal2;
      //cout<<"WeightTotal2= \n"<<WeightTotal2<<endl;
      //cout<<"M2= \n "<<M2<<endl;

      WeightZ3 = 1/(pow(MassZ12*MassZ12 - MZ_PDG*MZ_PDG,2) + pow(MZ_PDG*GammaZ_PDG,2));
      WeightW3 = 1/(pow(MassW0*MassW0 - MW_PDG*MW_PDG,2) + pow(MW_PDG*GammaW_PDG,2));
      //cout<<"WeightZ3= \n "<<WeightZ3<<endl;
      //cout<<"WeightW3= \n "<<WeightW3<<endl;      
      WeightTotal3 = WeightZ3*WeightW3;
      M3 = -1*WeightTotal3;
      //cout<<"WeightTotal3= \n"<<WeightTotal3<<endl;
      //cout<<"M3= \n "<<M3<<endl;
    
      //Make the choices about which lepton is originating from which boson
	    // if( (M1 < M2 && M1 < M3) || (MassZ01 != 0 && MassW2 != 0 && MassZ02 == 0 && MassZ12 == 0) ){
	//  i = 0; j = 1; k = 2; 
	//cout<<"Zlepton = 0,1; Wlepton = 2 \n";
	// }
	    // if( (M2 < M1 && M2 < M3) || (MassZ02 != 0 && MassW1 != 0 && MassZ01 == 0 && MassZ12 == 0) ){
	//        i = 0; j = 2; k = 1;
	//cout<<"Zlepton = 0,2; Wlepton = 1 \n";
	//}
	    // if( (M3 < M1 && M3 < M2) || (MassZ12 != 0 && MassW0 != 0 && MassZ01 == 0 && MassZ02 == 0) ){
	    //i = 1; j = 2; k = 0;
	//cout<<"Zlepton = 1,2; Wlepton = 0 \n";
	// }

      //obtain the four momentum from leptons originating from Z, W               
      //#Star8 // This assignment is wrong. this needs to be modified;
	    // FourMomentum Zlepton1 = dressedleptons[i].momentum();
	    //   FourMomentum Zlepton2 = dressedleptons[j].momentum();
	    //  FourMomentum Wlepton  = dressedleptons[k].momentum();
      //cout<<"Zlepton1 =\n"<<Zlepton1<<endl;                                                                                                                                     
      //cout<<"Zlepton2 =\n"<<Zlepton2<<endl;                                                                                                                                     
      //cout<<"Wlepton= \n"<<Wlepton<<endl;                                                                                                                                       
      //############################################################################################
     //Make the choices about which lepton is originating from which boson                                                                                                                                 
	    if( (M1 < M2 && M1 < M3) || (MassZ01 != 0 && MassW2 != 0 && MassZ02 == 0 && MassZ12 == 0) ){
	      i = 0; j = 1; k = 2;
	      cout<<"Case 1:"<<endl;
	      cout<<"Zlepton = 0,1; Wlepton = 2 \n";     
	      if (i== 0 && j ==1 && k==2){
	       Zlepton1 = dressedleptons[i].momentum();
	       Zlepton2 = dressedleptons[j].momentum();
	       Wlepton  = dressedleptons[k].momentum();
	       double Zlepton1_ParticleID = (dressedleptons[0]).pid();                                                                                                                         
	       double Zlepton2_ParticleID = (dressedleptons[1]).pid();
	       cout<<"Zlepton1_ParticleID =\n"<<Zlepton1_ParticleID<<endl;  
	       cout<<"Zlepton2_ParticleID =\n"<<Zlepton2_ParticleID<<endl;
	        }
	    }
	    if( (M2 < M1 && M2 < M3) || (MassZ02 != 0 && MassW1 != 0 && MassZ01 == 0 && MassZ12 == 0) ){
	       i = 0; j = 2; k = 1;
	      cout<<"Case 2:"<<endl;
	      cout<<"Zlepton = 0,2; Wlepton = 1 \n";               
	       if (i== 0 && j ==2 && k==1){
	       Zlepton1 = dressedleptons[i].momentum();
               Zlepton2 = dressedleptons[k].momentum();
	       Wlepton  = dressedleptons[j].momentum();
	       double Zlepton1_ParticleID = (dressedleptons[0]).pid();                                                                                                                         
	       double Zlepton2_ParticleID = (dressedleptons[2]).pid();
               cout<<"Zlepton1_ParticleID =\n"<<Zlepton1_ParticleID<<endl;
	       cout<<"Zlepton2_ParticleID =\n"<<Zlepton2_ParticleID<<endl;
	       }

	    }
	    if( (M3 < M1 && M3 < M2) || (MassZ12 != 0 && MassW0 != 0 && MassZ01 == 0 && MassZ02 == 0) ){
	      i = 1; j = 2; k = 0;
	      cout<<"Case 3:"<<endl;
	      cout<<"Zlepton = 1,2; Wlepton = 0 \n";                                                                                                                                                    
	      if (i== 1 && j ==2 && k==0){
		Zlepton1 = dressedleptons[j].momentum();
		Zlepton2 = dressedleptons[k].momentum();
		Wlepton  = dressedleptons[i].momentum();
		double Zlepton1_ParticleID = (dressedleptons[1]).pid();	
		double Zlepton2_ParticleID = (dressedleptons[2]).pid();
		cout<<"Zlepton1_ParticleID =\n"<<Zlepton1_ParticleID<<endl;
		cout<<"Zlepton2_ParticleID =\n"<<Zlepton2_ParticleID<<endl;

	       }

	    }
	    //##############################################################################################
	    // Zlepton1 = dressedleptons[j].momentum();
	    // Zlepton2 = dressedleptons[k].momentum();
	    // Wlepton  = dressedleptons[i].momentum();
      //Adding the new Feature - The Pt,eta, delta eta, Phi, Delta Phi, R and other information can be obtained from leptons which could also be interesting.
     
      //pT of leptons
      //Wlepton is the lepton originating from W ##Star B
      // double Wlepton_pt = (dressedleptons[k].momentum()).pt()*GeV;
      double Wlepton_pt = (Wlepton).pt()*GeV;
      cout<<"Wlepton_pt= \n"<<Wlepton_pt<<endl;
      // _h_Wlepton_pt->fill(Wlepton_pt, weight);
      // _h_Wlepton_pt->fill(Wlepton_pt>200?200:Wlepton_pt, weight);
      _h_Wlepton_pt->fill(Wlepton_pt>490?490:Wlepton_pt, weight);//overflow for Lepton from Leptonic_W
     
      //Zlepton1,2 are the leptons originating from Z                                                                                                                                                           
      //double Zlepton1_pt = (dressedleptons[i].momentum()).pt()*GeV; 
      //double Zlepton2_pt = (dressedleptons[j].momentum()).pt()*GeV;
      double Zlepton1_pt = (Zlepton1).pt()*GeV;
      double Zlepton2_pt = (Zlepton2).pt()*GeV;
      cout<<"Zlepton1_pt= \n"<<Zlepton1_pt<<endl;
      cout<<"Zlepton2_pt= \n"<<Zlepton2_pt<<endl;
      _h_Zlepton_pt->fill(Zlepton1_pt>490?490:Zlepton1_pt, weight);//StarB //overflow for Leptons from Z. Making a double entires per event.   
      // _h_Zlepton_pt->fill(Zlepton1_pt>200?200:Zlepton1_pt, weight);
      _h_Zlepton_pt->fill(Zlepton2_pt>490?490:Zlepton2_pt, weight);
      // _h_Zlepton_pt->fill(Zlepton2_pt>200?200:Zlepton2_pt, weight);
    
      //Theta between the two leptons originating from Z
      double theta_ll =((Zlepton1).theta() - (Zlepton2).theta());
      // (dressedleptons[i].momentum()+dressedleptons[j].momentum()).theta();
      // cout<<"Zlepton_theta_leplep= \n"<<theta_ll<<endl;
       _h_theta_ll->fill(theta_ll, weight);

       //Obtaining Eta and Delta Eta between l+l- coming from Z
       double Eta_lplus = (Zlepton1).eta();//Star#6                                                                                                                              
       cout<<"Eta_lplus= \n "<<Eta_lplus<<endl;
       double Eta_lminus = (Zlepton2).eta();                                                                                                                              
       cout<<"Eta_lminus= \n "<<Eta_lminus<<endl;
       double delta_Eta_lplm = abs(Eta_lplus - Eta_lminus); 
       cout<<"delta_Eta_lplm= \n "<<delta_Eta_lplm<<endl;
       _h_delta_Eta_lplm->fill(delta_Eta_lplm,weight); 
      
       //Obtaining Phi and Delta Phi between l+l- coming from Z                                                                                                                  
       double Phi_lplus = (Zlepton1).phi();//Star#7                                                                                                                              
       cout<<"Phi_lplus= \n "<<Eta_lplus<<endl;
       double Phi_lminus = (Zlepton2).phi();
       cout<<"Phi_lminus= \n "<<Phi_lminus<<endl;
       double delta_Phi_lplm = Pi - abs( abs(Phi_lplus - Phi_lminus) - Pi);
       cout<<"delta_Phi_lplm= \n "<<delta_Phi_lplm<<endl;                                                                                                                        
       _h_delta_Phi_lplm->fill(delta_Phi_lplm,weight);

      //obtain the four momentum of Z, W bosons ##starA
      FourMomentum Zboson   = dressedleptons[i].momentum()+dressedleptons[j].momentum();
      FourMomentum Wboson   = dressedleptons[k].momentum()+neutrinos[0].momentum();
      //cout<<"Zboson = \n"<<Zboson<<endl;
      //cout<<"Wboson = \n"<<Wboson<<endl;
      //Hadronic_W //Star#4
      Hadronic_W = Wjet1 + Wjet2;
      //cout<<"Hadronic_W = \n"<<Hadronic_W<<endl;

      //Obtain mass and transverse mass of W and Z bosons,along with phi and eta and fill into respective histos
      double Wboson_m = (Wboson).mass()*GeV;
      double Zboson_m=  (Zboson).mass()*GeV; 
      //  double Wboson_mT = sqrt( 2 * Wlepton.pT() * neutrinos[0].pt() * (1 - cos(deltaPhi(Wlepton, neutrinos[0]))) )/GeV;//W Boson Transverse mass
      //cout<<"Wboson_m  = \n"<<Wboson_m<<endl;    
      //cout<<"Zboson_m  = \n"<<Zboson_m<<endl;
      //cout<<"Wboson_mT = \n"<<Wboson_mT<<endl;
      _h_Wboson_m->fill(Wboson_m<71?71:Wboson_m, weight);//underflow for Invariant Mass for Leptonic_W
      _h_Wboson_m->fill(Wboson_m>99?99:Wboson_m, weight);//overflow for Invariant Mass for Leptonic_W 
      _h_Zboson_m->fill(Zboson_m<71?71:Zboson_m, weight);//underflow for Invariant Mass for Z
      _h_Zboson_m->fill(Zboson_m>99?99:Zboson_m, weight);//overflow for  Invariant Mass for Z 
      // _h_Wboson_mT->fill(Wboson_mT>100?100:Wboson_mT, weight);

      // relaxing these cuts
      //  if (fabs(Zboson.mass()-MZ_PDG)>=10.)  vetoEvent;
      // if (Wboson_mT<=30.)                   vetoEvent;
      // if (Wlepton.pT()<=20.)                vetoEvent;
      // if (deltaR(Zlepton1,Zlepton2) < 0.2)  vetoEvent;
      // if (deltaR(Zlepton1,Wlepton)  < 0.3)  vetoEvent;
      // if (deltaR(Zlepton2,Wlepton)  < 0.3)  vetoEvent;
     
      //Obtain pT of W and Z bosons and fill into respective histos   
      double Wboson_pt = (Wboson).pt()*GeV;
      double Zboson_pt = (Zboson).pt()*GeV;
      //cout<<"Wboson_pt = \n"<<Wboson_pt<<endl;
      //cout<<"Zboson_pt = \n"<<Zboson_pt<<endl;
      // _h_Wboson_pt->fill(Wboson_pt, weight);
      // _h_Zboson_pt->fill(Zboson_pt, weight);
      _h_Wboson_pt->fill(Wboson_pt>999?999:Wboson_pt, weight); //Overflow for pT Leptonic_W                                                                                                                
      _h_Zboson_pt->fill(Zboson_pt>999?999:Zboson_pt, weight);//Overflow for pT of Z                                                  
      

      //Calculating eta, Phi and deltaPhi,deltaEta,deltaR of and between the three bosons
     
      //Star#1//Calculating Eta                                                                                                                                                                     
      double eta1 = (Wboson).eta();
      //cout<<"Wboson_eta= \n"<<eta1<<endl;
      _h_Wboson_eta->fill(eta1,weight);
     
      double eta2 = (Zboson).eta();
      //cout<<"Zboson_eta= "<<eta2<<endl;                                                                                                                                                        
      _h_Zboson_eta->fill(eta2,weight);  

      double eta3 = (Hadronic_W).eta();//Star#5
      //cout<<"Hadronic_W_eta= \n "<<eta3<<endl;
      _h_Hadronic_W_eta->fill(eta3,weight);

      //Calculating delta_Eta 

      //Eta between leptonic_W and Z
      // double delta_first_Eta = (Wboson + Zboson).deltaEta();
      double delta_first_Eta = abs(eta1 - eta2);
      // cout<<"delta_first_Eta"<<delta_first_Eta<<endl;
      _h_delta_first_Eta->fill(delta_first_Eta,weight);

      //Eta between Hadronic_W and Leptonic_W                                                                                                                                                              
      // double delta_second_Eta = (Hadronic_W + Wboson).deltaEta();
      double delta_second_Eta =abs( eta3 - eta1);
      //cout<<"delta_second_Eta=  \n"<<delta_second_Eta<<endl;
      _h_delta_second_Eta->fill(delta_second_Eta,weight);

      //Eta between Hadronic_W and Z                                                                                                                                                                       
      // double delta_third_Eta = (Hadronic_W + Zboson).deltaEta();
      double delta_third_Eta =abs( eta3 - eta2);
      //cout<<"delta_third_Eta= \n "<<delta_third_Eta<<endl;
      _h_delta_third_Eta->fill(delta_third_Eta,weight);
    
      //Obtaining Phi for bosons and then calculating Delta_Phi //Star#3
      //Calculating various phi between the 3 bosons and obtaining their individual Phi as ph1,phi2, phi3 respectively.Then obtain differences between them which will be the angle between the 3 bosons.  
      //first_phi is the angle between leptonically decaying W and Z, second_phi is the angle between the two W, third phi is the angle between hadronically decaying W and Z.                             
     
      double phi1 = ( Wboson).phi();
      //cout<<"Wboson_Phi= \n "<<phi1<<endl;
      _h_Wboson_Phi->fill(phi1,weight);
      
      double phi2 = ( Zboson).phi();
      //cout<<"Zboson_Phi= \n "<<phi2<<endl;
      _h_Zboson_Phi->fill(phi2,weight);
       
      double phi3 = ( Hadronic_W).phi();
      //cout<<"Hadronic_W_Phi= \n "<<phi3<<endl;
      _h_Hadronic_W_Phi->fill(phi3,weight);
      
      //Calculating delta_Phi between leptonic_W and Z  
      // double delta_first_Phi   = (Wboson + Zboson).deltaPhi();
     
      // double delta_first_Phi_old  = phi1 - phi2;
      double delta_first_Phi  = Pi - abs(abs( phi1 - phi2) - Pi);
      //cout<<"delta_first_Phi_corrected  = "<<delta_first_Phi_corrected<<endl;
      // cout<<"delta_first_Phi =  \n"<<delta_first_Phi<<endl;
      _h_delta_first_Phi->fill(delta_first_Phi,weight);

      //Calculating delta_Phi between Hadronic_W and Leptonic_W
      // double delta_second_Phi  = (Wboson + Hadronic_W).deltaPhi();
      // double delta_second_Phi = phi3 - phi1;
      double delta_second_Phi  = Pi - abs(abs( phi3 - phi1) - Pi);
      // cout<<"delta_second_Phi= \n"<<delta_second_Phi<<endl;
      _h_delta_second_Phi->fill(delta_second_Phi,weight);
    
      //Calculating delta_Phi between Hadronic_W and Z 
      //double delta_third_Phi  = (Zboson + Hadronic_W).deltaPhi();
      //  double delta_third_Phi = phi3 - phi2;
      double delta_third_Phi  = Pi - abs(abs( phi3 - phi2) - Pi);  
      // cout<<"delta_third_Phi=  \n"<<delta_third_Phi<<endl;
      _h_delta_third_Phi->fill(delta_third_Phi,weight);

      //Can write deltaR here by formula , but sebastian suggested to use a method from RIVET for calculating delta eta and delta R.
      //Note: This is the delta_R for the three bosons!
      double delta_R_first = sqrt(  (delta_first_Phi*delta_first_Phi) + (delta_first_Eta*delta_first_Eta) );    
      //cout<<"delta_R_first = \n"<<delta_R_first<<endl;
      // _h_delta_R_first->fill(delta_R_first,weight);
      _h_delta_R_first->fill(delta_R_first>6.8?6.8:delta_R_first,weight); 
 
     
      double delta_R_second = sqrt(  (delta_second_Phi*delta_second_Phi) + (delta_second_Eta*delta_second_Eta) );
      //cout<<"delta_R_second = \n "<<delta_R_second<<endl;
      // _h_delta_R_second->fill(delta_R_second,weight);
      _h_delta_R_second->fill(delta_R_second>6.9?6.9:delta_R_second,weight);
 
      double delta_R_third = sqrt(  (delta_third_Phi*delta_third_Phi) + (delta_third_Eta*delta_third_Eta) );
      //cout<<"delta_R_third = \n "<<delta_R_third<<endl;
      // _h_delta_R_third->fill(delta_R_third,weight);
      _h_delta_R_third->fill(delta_R_third>6.8?6.8:delta_R_third,weight);
       

      //Its stupid to do these variables for WZ system in total. Can use WZ_pt here to get H_t by removing the neutrino component.Can also get the x and y component of this pt. Note: Need to multiply by       GeV to get the right values !Need to calculate the ET_missing! Check if rivet has a method for that.
      //Obtain Values for WZ system in total ##starC
      double H_t = Zlepton1.pt()*GeV + Zlepton2.pt()*GeV + Wlepton.pt()*GeV;
      cout<<"H_t =  \n"<<H_t<<endl; 
      // double WZ_px = Zlepton1.px() + Zlepton2.px() + Wlepton.px() + neutrinos[0].px();
      // double WZ_py = Zlepton1.py() + Zlepton2.py() + Wlepton.py() + neutrinos[0].py();
      // double mTWZ  = sqrt( pow(WZ_pt, 2) - ( pow(WZ_px, 2) + pow(WZ_py,2) ) )/GeV;
      // double AbsDeltay = Zboson.rapidity()-Wlepton.rapidity();
      // double WZ_m  = (dressedleptons[i].momentum()+dressedleptons[j].momentum()+dressedleptons[k].momentum()).mass() *GeV;

      //   cout<<"WZ_pt =  \n"<<WZ_pt<<endl;
      // cout<<"WZ_px =  \n"<<WZ_px<<endl;
      // cout<<"WZ_py =  \n"<<WZ_py<<endl;
      // cout<<"mTWZ  =  \n"<<mTWZ<<endl;
      // cout<<"AbsDeltay=\n"<<AbsDeltay<<endl;
      // cout<<"WZ_m  =  \n"<<WZ_m<<endl;
      _h_H_t->fill(H_t>490?490:H_t, weight);
      //  _h_WZ_m->fill(WZ_m, weight);
      // _h_mTWZ->fill(mTWZ, weight);

      //Fill histos depending on type of events based on number of electrons or muons or both
      //      if (EventType == 3) _h_eee->fill(8000., weight);
      // if (EventType == 2) _h_mee->fill(8000., weight);
      // if (EventType == 1) _h_emm->fill(8000., weight);
      // if (EventType == 0) _h_mmm->fill(8000., weight);
      // _h_fid->fill(8000., weight);

      //      if (EventCharge == 1) {

      //if (EventType == 3) _h_eee_Plus->fill(8000., weight);
      // if (EventType == 2) _h_mee_Plus->fill(8000., weight);
      // if (EventType == 1) _h_emm_Plus->fill(8000., weight);
      // if (EventType == 0) _h_mmm_Plus->fill(8000., weight);
      //	_h_fid_Plus->fill(8000., weight);
	
	//	_h_deltay_Plus->fill(AbsDeltay, weight);
	//	_h_deltay_norm_Plus->fill(AbsDeltay, weight);
        //_h_Deltay_Plus->fill(AbsDeltay, weight);
        //_h_Deltay_Plus_norm->fill(AbsDeltay, weight);
	//   fillWithOverflow("ZpT_Plus", Zboson.pT()/GeV, 220, weight);
	// fillWithOverflow("WpT_Plus", Wboson.pT()/GeV, 220, weight);
	// fillWithOverflow("mTWZ_Plus", mTWZ, 600, weight);
        //fillWithOverflow("pTv_Plus", neutrinos[0].pt(), 90, weight);
	// fillWithOverflow("ZpT_Plus_norm", Zboson.pT()/GeV, 220, weight);
        //fillWithOverflow("pTv_Plus_norm", neutrinos[0].pt()/GeV, 90, weight);

      // } else {

      //if (EventType == 3) _h_eee_Minus->fill(8000., weight);
      // if (EventType == 2) _h_mee_Minus->fill(8000., weight);
      //if (EventType == 1) _h_emm_Minus->fill(8000., weight);
      //if (EventType == 0) _h_mmm_Minus->fill(8000., weight);
      //_h_fid_Minus->fill(8000., weight);
      // }
      // _h_deltay->fill(AbsDeltay, weight);
       // _h_deltay_norm->fill(AbsDeltay, weight);
      }

    void finalize() {

      /// @todo Normalise, scale and otherwise manipulate histograms here
      //    scale(_h_2l2j, crossSection()/sumOfWeights()/femtobarn); // norm to cross section
    }

    //@}
    //  private:
  public: 
   
    //Defining 1D histograms for Z
    // Histo1DPtr _h_eee;
    // Histo1DPtr _h_mee;       
    // Histo1DPtr _h_emm; 
    // Histo1DPtr _h_mmm;   
    // Histo1DPtr _h_fid;     
    // Histo1DPtr _h_eee_Plus  ;
    // Histo1DPtr _h_mee_Plus  ;
    // Histo1DPtr _h_emm_Plus  ; 
    // Histo1DPtr _h_mmm_Plus  ; 
    // Histo1DPtr _h_fid_Plus  ; 
    // Histo1DPtr _h_eee_Minus ; 
    // Histo1DPtr _h_mee_Minus ; 
    // Histo1DPtr _h_emm_Minus; 
    // Histo1DPtr _h_mmm_Minus ; 
    // Histo1DPtr _h_fid_Minus ; 
    // Histo1DPtr _h_Deltay_Plus;
    // Histo1DPtr _h_Deltay_Plus_norm;
    // Histo1DPtr _h_Deltay_Minus;
    // Histo1DPtr _h_Deltay_Minus_norm;   
    // Histo1DPtr _h_Deltay;
    // Histo1DPtr _h_Deltay_norm;
    Histo1DPtr _h_Wboson_m;
    Histo1DPtr _h_Zboson_m;
    Histo1DPtr _h_Wboson_mT;
    // Histo1DPtr _h_Zboson_mT;
    Histo1DPtr _h_Wboson_pt;
    Histo1DPtr _h_Zboson_pt;
    Histo1DPtr _h_Wboson_eta;
    Histo1DPtr _h_Zboson_eta;
    Histo1DPtr _h_weights;
    Histo1DPtr _h_H_t;
    Histo1DPtr _h_delta_Eta_lplm;
    Histo1DPtr _h_delta_Phi_lplm;
    Histo1DPtr _h_Wjj_m;
    Histo1DPtr _h_singlejet_m;
    Histo1DPtr _h_Wjj_pt;
    Histo1DPtr _h_Wjj_Njets;
    Histo1DPtr _h_Wjj_eta;
    Histo1DPtr _h_Wjj_m_best;
    Histo1DPtr _h_Ntotalevents;
    Histo1DPtr _h_Wlepton_pt;
    Histo1DPtr _h_Zlepton_pt;
    Histo1DPtr _h_theta_ll;
    Histo1DPtr _h_Wboson_Phi;
    Histo1DPtr _h_Hadronic_W_Phi;
    Histo1DPtr _h_Zboson_Phi;
    Histo1DPtr _h_delta_first_Eta;  
    Histo1DPtr _h_delta_second_Eta;
    Histo1DPtr _h_delta_third_Eta;
    Histo1DPtr _h_delta_first_Phi;
    Histo1DPtr _h_delta_second_Phi;
    Histo1DPtr _h_delta_third_Phi;
    Histo1DPtr _h_delta_R_first;
    Histo1DPtr _h_delta_R_second;
    Histo1DPtr _h_delta_R_third;
    Histo1DPtr  _h_Hadronic_W_eta;
    Histo1DPtr _h_Nlepevents_after;
    //  Histo1DPtr _h_Nlepevents_before;
    Histo1DPtr _h_Njetevents;
    Histo1DPtr _h_Nlep_before;
    Histo1DPtr _h_Nlep_after;
    Histo1DPtr _h_Nnu;
    double MZ_PDG = 91.1876;
    double MW_PDG = 83.385;
    double GammaZ_PDG = 2.4952;
    double GammaW_PDG = 2.085;
    double Pi = 3.14159265;
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(WWZ);
}
