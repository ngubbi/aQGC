#!/bin/usr/python 

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

rootsys = str(os.environ['ROOTSYS'])

listRootFiles = ""
histoName = ""

try:
    listRootFiles = str(sys.argv[1])
    histoName = str(sys.argv[2])
except IndexError:
    print "Argument missing."
    print "\tUsage==> python run.py listRootFiles histoName"
    sys.exit(1)
    pass

if not os.path.exists("./"+listRootFiles): 
    print "Input "+str(sys.argv[1])+" not found, aborting..."
    sys.exit(1)
    pass

print "Compiling macros..."
cmpFailure = 0
cmpFailure += gROOT.ProcessLine(".L giveMePlots.C+")
if cmpFailure:
    print "Compilation failure, aborting..."
    sys.exit(1)
    pass

# Set log scale on the y axis?
LogY=False
#LogY=True

giveMePlots( listRootFiles, histoName, LogY )

#can add other text file in this function too, to increase functionality!
