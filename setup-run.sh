This document lists the various steps and softwares needed for setup and also gives details on how to obtain it-either for your your laptop or if your are accessing it from Bonn on lxplus.

#Tring Tring: Need to seek permission to use and access all these softwares on BAF and also need to obtain a uni-ID from HRZ on the basis of which IT-support in PI bonn will make a account for you and will then let you run the software on BAF

# 1.Getting MadGraph (v2.1.1):

#For laptop:
Download from website http://madgraph.hep.uiuc.edu/ . A really nice and informative twiki for installation is at https://twiki.cern.ch/twiki/bin/view/CMSPublic/MadgraphTutorial. 

#For Bonn to use on lxplus:(So for MadGraph on an SLC6 machine (lxplus, old lxbatch machines), you would do):                                                                                              
setupATLAS
lsetup "lcgenv -p LCG_93 x86_64-slc6-gcc7-opt madgraph5amc 2.6.1"

After downlaod and installation, go to where you installed the MadGraph folder
> cd MG5_aMC_v2_6_1
to start the program:
> ./bin/mg5
or
>./bin/mg5_aMC


# 2.Getting Mathematica

#For laptop: 
Contact Mr. Andreas Weisskirchen at mathematica@th.physik.uni-bonn.de or wisskirc@th.physik.uni-bonn.de and request for Mathematica licence. Then from webpage http://mathematica.physik.uni-bonn.de/ can  obtain tlink to downlaod and install Mathematica on your laptop.

#For Bonn to use on lxplus: 
module load mathematica/11.3.0 (NOTE: CANNOT use Mathematica on lxplus, need to run it locally)

# 3.Feynrules

#For laptop:  
Download UFO models from http://feynrules.irmp.ucl.ac.be/wiki/AnomalousGaugeCoupling
Some UFO model examples :
1.SM_LM0123_UFO,
2.SM_LS0_LS1_UFO,SM_LT012_UFO(for FT aQGC variables)

#For Bonn to use on lxplus: 
Quite similar to how you do on laptop, since you are running mathematica locally

#3. Getting Python
#For laptop: 
Need python version 2.6 or 2.7
check by typing
> python
version number should appear if installed
cntrl^D to exit

# 4. Getting Pythia,Delphes 

#For laptop:  
Download pythia v8 tarball from home.thep.lu.se/Pythia (v8.2) 
Then can do the following commands for Pythia: 
> tar -xf pythia-pgs_V2.4.3.tar.gz
> cd pythia-pgs
> sudo make

Then can do the following commands for Delphes:

> curl -O http://cp3.irmp.ucl.ac.be/downloads/Delphes-3.2.0.tar.gz
> tar -xf Delphes-3.2.0.tar.gz
> cd Delphes-3.2.0/
> make

In the file input/mg5_configuration.txt, change the delphes_path variable on line 122 from

delphes_path = ./Delphes
to
delphes_path = ./Delphes-3.2.0 or whatever is the version of your downlaod of delphes

#For Bonn to use on lxplus: 
most of the software, can be set up using the lcgenv way. To set up the available software on lxplus, BAF1 or BAF2 you could do:

setupATLAS
lsetup "lcgenv -p LCG_93 x86_64-slc6-gcc7-opt madgraph5amc 2.6.1.atlas"
lsetup "lcgenv -p LCG_93 x86_64-slc6-gcc7-opt pythia8 235"
lsetup "lcgenv -p LCG_93 x86_64-slc6-gcc7-opt delphes"

Madgraph can then be started using "mg5_aMC". 

#For Linux: 
install pythia/pgs:
mg5_aMC> install pythia-pgs
mg5_aMC> install ExRootAnalysis (see below) 
[[
Will need ROOT for this. After installation, will need to add some lines into the .bash_profile (.bashrc for windows) file in order for it to work. To do this open the file with
>nano .bash_profile
and then write in the following lines
export ROOTSYS=/usr/local
export PATH=$PATH:$ROOTSYS/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib
export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$ROOTSYS/lib
 ]]
mg5_aMC> install MadAnalysis (may need gosthscript but you may be able to compile successfully without it  http://www.macupdate.com/app/mac/9980/gpl-ghostscript.)
mg5_aMC> install Delphes
 
# 5. Getting MadAnalysis

#For laptop: 
The tarball for Madanalysis can be found at https://launchpad.net/madanalysis5. Can just unpacl tarball and install using the README file (pretty simple steps really) and start using them.

#For Bonn to use on lxplus: 
Unfortunately, MadAnalysis does not seem to be centrally provided by ATLAS. Although, it may be provided through Madgraph in the future according to https://madanalysis.irmp.ucl.ac.be/wiki/tutorials.    It seems like, for now, it should be possible to use the software by simply downloading a tar-ball and unpacking it.

# Andrea still needs to provide licence for installing Mathematica on Mac
# I need to obtain some new ID from HRZ to get permission to use these softwares on BAF, which I need to get tomorrow. 
